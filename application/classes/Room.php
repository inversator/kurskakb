<?php

# Общий класс для полезных функций

class Room
{

    public static function syspath()
    {
        defined('SYSYPATH') || die('Прямой доступ запрещен');
    }

    // Перевод размера данных
    public static function dataSize($Bytes)
    {
        $Type = array("", "К", "М", "Г", "Т");
        $counter = 0;
        while ($Bytes >= 1024) {
            $Bytes /= 1024;
            $counter++;
        }
        return ("" . substr($Bytes, 0, 3) . " " . $Type[$counter] . "Б");
    }

    public static function die_dump($value)
    {
        echo '<pre>';
        var_dump($value);
        echo '</pre>';

        die();
    }

    public static function subDesc($text, $length = 50)
    {
        if (strlen($text) > $length) return UTF8::substr($text, 0, $length) . "...";
        else return $text;
    }

    public static function subTitle($text, $length = 60)
    {
        if (strlen($text) > $length) return UTF8::substr($text, 0, $length) . "...";
        else return $text;
    }

    /*
     * В случае отстутствия картинки выводится заглушка подходящих размеров
     */
    public static function showImg($img, $width = 300, $height = 300)
    {
        if (!$img) {
            $img = $width . 'x' . $height . '.png';
        }
        return $img;
    }

    public static function noImg($img, $width = 300, $height = 300)
    {
        if (!$img) {
            $img = 'no-img.png';
        }
        return $img;
    }
}