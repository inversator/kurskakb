<?php

class Model_Menu extends Model_Base
{
    protected $_table_name = 'menu';
    protected $_table_columns = array(
        'id' => NULL,
        'name' => NULL,
        'alias' => NULL,
        'off' => NULL,
        'comment' => NULL,
    );

    public function get($id)
    {
        $sql = "SELECT * FROM ".$this->_table_name." WHERE id = :id LIMIT 1";
        $result = DB::query(1, $sql)->param(':id', $id)->execute()->as_array();

        return $result[0];
    }

    public function getActive($type)
    {
        $sql = "SELECT * FROM ".$this->_table_name." WHERE off <> '1' AND type =:type";

        return DB::query(1, $sql)->param(':type', $type)->execute()->as_array();
    }
}