<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Unit extends Model_Base
{
    protected $_table_name = 'units'; //Если имя иное
    //protected $_primary_key= 'id';
    protected $_table_columns = array(
        'id' => NULL,
        'meta_k' => NULL,
        'meta_d' => NULL,
        'bTitle' => NULL,
        'pTitle' => NULL,
        'desc' => NULL,
        'text' => NULL,
        'off' => NULL,
        'url' => NULL,
        'date' => NULL,
        'recommended' => NULL,
    );

    public function getParent($id)
    {
        return DB::select('c.url')
            ->from(array('relation','r'))
            ->join(array('categories','c'))
            ->on('c.id','=','r.par_record')
            ->where('r.dot_record','=',$id)
            ->and_where('dot_essence','=','unit')
            ->and_where('par_essence','=','category')
            ->execute()
            ->as_array();
    }

    public function getProperties($id_unit)
    {
        $v = 'property_values';
        $p = 'properties';

        return DB::select($v.'.value', $v.'.pos', $v.'.id', $p.'.alias',
                    $v.'.id_prop')
                ->from($v)
                ->join($p)
                ->on($v.'.id_prop', '=', $p.'.id')
                ->where($v.'.id_unit', '=', $id_unit)
                ->order_by($v.'.pos')
                ->execute()
        ;
    }

    public function getRecommended()
    {
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.id,
                    u.meta_k, 
                    u.meta_d, 
                    u.bTitle,
                    u.pTitle, 
                    u.desc,
                    u.text,
                    u.url,
                    i.name as image,
                    p.value as price

                    FROM units u
                    
                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1

                    WHERE u.off = 0
                    AND u.recommended = 1
                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
               ";
        return DB::query(1, $sql)->execute();
    }

    public function getLast()
    {
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.id,
                    u.meta_k, 
                    u.meta_d, 
                    u.bTitle,
                    u.pTitle, 
                    u.desc,
                    u.text,
                    u.url,
                    u.date,
                    i.name as image,
                    p.value as price

                    FROM units u
                    
                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1

                    WHERE u.off = 0

                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
                ORDER BY t.date DESC
                LIMIT 9
               ";
        return DB::query(1, $sql)->execute();
    }

    public function relationUnits($id)
    {
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.id,
                    u.meta_k, 
                    u.meta_d, 
                    u.bTitle,
                    u.pTitle, 
                    u.desc,
                    u.text,
                    u.url,
                    i.name as image,
                    p.value as price

                    FROM units u
                    JOIN relation r 

                    ON r.par_essence = 'unit' 

                    AND r.dot_essence = 'unit' 
                    AND r.par_record = u.id
                    AND r.dot_record = :id

                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1

                    WHERE u.off = 0

                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
               ";
        //mysql_query($sql) or die(mysql_error());
        return DB::query(1, $sql)->param(':id', $id)->execute();
    }

    public function getImages($id)
    {
        return DB::select('i.name', 'i.main')
                ->from(array('images', 'i'))
                ->where('id_es', '=', $id)
                ->and_where('essence', '=', 'unit')
                ->execute();
    }

    public function getPrice($id)
    {
        $result = DB::select('value')
            ->from('property_values')
            ->where('id_prop', '=', '1')
            ->and_where('id_unit', '=', $id)
            ->execute()
            ->as_array();

        return isset($result[0]['value']) ? $result[0]['value'] : 0;
    }

    protected function rules()
    {
        return array(
            'pTitle' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'bTitle' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'meta_k' => array(
                array('max_length', array(':value', 255)),
            ),
            'meta_d' => array(
                array('max_length', array(':value', 255)),
            ),
            'url' => array(
                array('not_empty'),
                array('max_length', array(':value', 100)),
                array('unic_field', array(':value', ':field', 'unit', parent::$specimen['id']))
            ),
        );
    }
}