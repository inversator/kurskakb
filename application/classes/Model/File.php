<?php

/**
 * Created by PhpStorm.
 * User: maa
 * Date: 24.03.2017
 * Time: 15:55
 */
class Model_File extends ORM
{

    protected $_table_columns = array(
        'id' => 'id',
        'file' => 'file',
        'type' => 'type',
        'essence' => 'essence',
        'id_es' => 'id_es',
        'size' => 'size',
        'description' => 'description',
    );


    public function labels()
    {
        return array(
            'file' => 'файл',
            'description' => 'Описание',
        );
    }

    public function filters()
    {
        return array(
            TRUE => NULL,
            'description', array(
                array('trim'),
            ),
        );
    }

    public function rules()
    {

        return array(
            'file' => array(
                array('Upload::valid'),
                array('Upload::not_empty'),
                array('Upload::type', array(':value', array('jpg', 'jpeg', 'png', 'gif', 'zip', 'pdf', 'doc', 'docx', 'xls'))),
                array(array($this, 'file_save'), array(':value'))
            ),
        );
    }

    private function uploads_dir()
    {
        return DOCROOT . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR;
    }

    public function file_save($file)
    {

        // upload file
        $uploaded = Upload::save($file, $file['name'], $this->uploads_dir());

        // if uploaded set file name to save to database
        if ($uploaded) {
            // set file name
            $this->set('file', $file['name']);

            // set file type
            $this->set('type', strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)));

            // set file size
            $this->set('size', $file['size']);
        }

        // return result
        return $uploaded;
    }

    public function desc($id, $desc)
    {
        return Db::update('files')->set(array('description' => $desc))->where('id','=', $id)->execute();
    }


}