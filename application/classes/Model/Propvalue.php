<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_propvalue extends Model_Base
{
    protected $_table_name = 'property_values';
    protected $_essence = 'value';
    protected $_table_columns = array(
        'id_prop' => NULL,
        'value' => NULL,
        'id_unit' => NULL,
        'pos' => NULL
    );

}