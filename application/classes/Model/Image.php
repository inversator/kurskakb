<?php

class Model_Image extends Model_Base
{
    protected $_table_name = 'images';
    protected $_table_columns = array(
        'id' => NULL,
        'name' => NULL,
        'essence' => NULL,
        'id_es' => NULL,
    );

    public function getImage($essence, $id_es)
    {
        $sql = "SELECT * FROM ".$this->_table_name." WHERE essence = :essence AND id_es = :id_es";
        $result = DB::query(1, $sql)
                ->param(':essence', $essence)
                ->param(':id_es', $id_es)
                ->execute()->as_array();

        return $result;
    }

    public function makeMain($id, $id_es)
    {
        $sql = "UPDATE images SET main=0 WHERE id_es = :id_es"; // Делаем все изображения не главными
        DB::query(3, $sql)->param(':id_es', $id_es)->execute();

        $sql = "UPDATE images SET main=1 WHERE id_es = :id_es AND id = $id"; // Делаем выбранное изображение главным
        DB::query(3, $sql)->param(':id_es', $id_es)->param(':id', $id)->execute();
    }

    public function insertDesc($id, $desc)
    {
        $sql = "UPDATE images SET `desc` = :desc WHERE id = :id";
        DB::query(3, $sql)->param(':id', $id)->param(':desc',$desc)->execute();
    }
}