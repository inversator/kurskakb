<?php

/**
 * Created by PhpStorm.
 * User: maa
 * Date: 26.04.2017
 * Time: 12:57
 */
class Model_Question extends Model_Base
{
    protected $_table_name = 'questions';
    protected $_table_columns = array(
        'id' => NULL,
        'date' => NULL,
        'user' => NULL,
        'question' => NULL,
        'manager' => NULL,
        'm_func' => NULL,
        'answer' => NULL,
    );

    protected function rules()
    {

        return array(
            'user' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'manager' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'm_func' => array(
                array('max_length', array(':value', 255)),
            )
        );
    }

    public function takeWithPag($limit, $offset)
    {

        return Db::select()->from($this->_table_name)->limit($limit)->offset($offset)->order_by('date','DESC')->execute();

    }
}