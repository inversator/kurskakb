<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_fixedprop extends Model_Base
{
    protected $_table_name = 'fixed_property_values';
    protected $_essence = 'value';
    protected $_table_columns = array(
        'id_prop' => NULL,
        'value' => NULL,
    );

}