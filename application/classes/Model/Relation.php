<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Relation extends Model_Base
{
    protected $_table_name = 'relation';
    protected $_table_columns = array(
        'par_essence' => NULL,
        'dot_essence' => NULL,
        'par_record' => NULL,
        'dot_record' => NULL
    );

    public function unlink($id_essence, $essence)
    {
        return DB::delete($this->_table_name)
            ->or_where_open('par_essence','=',$essence)
                ->and_where('par_record', '=', $id_essence)
            ->or_where_close('dot_essence','=',$essence)
                ->and_where('dot_record', '=', $id_essence)
            ->execute();
    }


}