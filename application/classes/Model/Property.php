<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Property extends Model_Base
{
    protected $_table_name = 'properties';
    protected $_essence = 'property';
    protected $_table_columns = array(
        'id' => NULL,
        'name' => NULL,
        'alias' => NULL,
        'static' => NULL,
        'fixed' => NULL
    );

    public function getFixedValues($id)
    {
        return DB::select()->from('fixed_property_values')->where('id_prop',
                '=', $id)->execute();
    }

    public function addFixed($fields)
    {
        $values = $this->confirmKeys($fields);
        $keys = array_keys($values);
        return DB::insert('fixed_property_values', $keys)->values($values)->execute();
    }

    protected function rules()
    {

        return array(
            'name' => array(
                array('not_empty'),
                array('english_num'),
                array('max_length', array(':value', 255)),
                array('unic_field', array(':value', ':field', $this->_essence, parent::$specimen['id']))
            ),
            'alias' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
                array('unic_field', array(':value', ':field', $this->_essence, parent::$specimen['id']))
            )
        );
    }
}