<?php
/**
 * Created by PhpStorm.
 * User: maa
 * Date: 07.04.2017
 * Time: 13:54
 */

class Model_Dealer extends Model_Base {

    protected $_table_name = 'dealers';
    protected $_table_columns = array(
        'id' => NULL,
        'name',
        'phone',
        'email',
        'factory',
        'address',
        'off',
        'type',
        'latitude',
        'longitude',
        'site'
    );



}