<?php

class Model_Base extends Model
{
    /**
     * Инициализирую образец объекта по ID для обращения к параметрам
     */
    public static $specimen = NULL;

    public static function factory($name, $id = NULL)
    {
        $class = parent::factory($name);

        if ($id) {
            //Если есть ID создаем образец объекта
            self::$specimen = $class->get($id);
        }
        return $class;
    }

    public function add($fields)
    {

        $values = $this->confirmKeys($fields);
        $valueKeys = array_keys($values);

        return DB::insert($this->_table_name, $valueKeys)->values($values)->execute();
    }

    public function count()
    {
        $result = DB::select(array(DB::expr('COUNT(*)'), 'count'))->from($this->_table_name)->execute()->as_array();
        return $result[0]['count'];
    }

    public function countWhere($fields)
    {
        $values = $this->confirmKeys($fields);

        $query = DB::select(array(DB::expr('COUNT(*)'), 'count'))->from($this->_table_name);
        $query = $this->andWhereArr($query, $values, 0);

        $result = $query->execute()->as_array();
        return $result[0]['count'];
    }

    public function countWhereParent($parEss, $parId)
    {
        $query = DB::select(array(DB::expr('COUNT(*)'), 'count'))->from(array($this->_table_name, 'a'));
        $query->join(array('relation', 'r'))
            ->on('a.id', '=', 'r.dot_record')
            ->where('r.par_essence', '=', $parEss)
            ->and_where('r.par_record', '=', $parId);

        $result = $query->execute()->as_array();
        return $result[0]['count'];
    }



    public function uniqField($field, $id)
    {
        $values = $this->confirmKeys($field);

        $query = DB::select(array(DB::expr('COUNT(*)'), 'count'))->from($this->_table_name);
        $query = $this->andWhereArr($query, $values, 0);

        $result = $query->and_where('id', '<>', $id)->execute()->as_array();
        return (BOOL)!$result[0]['count'];
    }

    public function andWhereArr($query, $fields, $fiest = 1)
    {
        foreach ($fields as $field => $value) {
            if ($fiest) {
                $query->where($field, '=', $value);
                $fiest = 0;
            }
            $query->and_where($field, '=', $value);
        }
        return $query;
    }

    public function getAllLim($offset, $limit)
    {
        return DB::select()->from($this->_table_name)->limit($limit)->offset($offset)->order_by('id', 'DESC')->execute();
    }

    public function getParLim(array $par, array $pag)
    {
        return DB::select()->from(array($this->_table_name, 'a'))
            ->join(array('relation', 'r'))
            ->on('a.id', '=', 'r.dot_record')
            ->where('r.par_essence', '=', $par[0])
            ->and_where('r.par_record', '=', $par[1])
            ->limit($pag[1])
            ->offset($pag[0])
            ->order_by('id', 'DESC')
            ->execute();
    }

    public function getAll()
    {
        $sql = "SELECT * FROM " . $this->_table_name;
        return DB::query(1, $sql)->execute();
    }
    public function getAllOrder($order)
    {
        $sql = "SELECT * FROM " . $this->_table_name . " ORDER BY ".$order;
        return DB::query(1, $sql)->execute();
    }

    public function getAllActive()
    {
        $sql = "SELECT * FROM " . $this->_table_name . " WHERE off = 0";
        return DB::query(1, $sql)->execute();
    }

    public function get($id)
    {
        $sql = "SELECT * FROM " . $this->_table_name . " WHERE id = :id LIMIT 1";
        $result = DB::query(1, $sql)->param(':id', $id)->execute()->as_array();

        return count($result) ? $result[0] : 0;
    }

    public function getOnUrl($slug)
    {
        $sql = "SELECT * FROM " . $this->_table_name . " WHERE url = :slug LIMIT 1";
        $result = DB::query(1, $sql)->param(':slug', $slug)->execute()->as_array();

        return count($result) ? $result[0] : 0;
    }

    /*
     *  Получение одной колонки всех записей getColumn('field');
     */

    public function getColumn($column)
    {
        $result = DB::select($column)->from($this->_table_name)->execute();

        $list[$column] = array();
        foreach ($result as $arr) {
            $list[$column][] = $arr[$column];
        }

        return $list;
    }

    /*
     *  Получение нескольких колонок всех записей getColumns(array('field1', 'field2));
     */

    public function getColumns($array)
    {
        return $array;
    }

    /*
     * Получение 2-х колонок в виде ключ => значение
     */

    public function getKeyValue($key, $value)
    {
        $result = DB::select($key, $value)->from($this->_table_name)->execute();
        $list = array();
        foreach ($result as $row) {
            $list[$row[$key]] = $row[$value];
        }
        return $list;
    }

    /*
     * Одна колонка по id
     */

    public function getOneColumn($id, $column)
    {
        return Db::select($column)
            ->from($this->_table_name)
            ->where('id', '=', $id)
            ->execute();
    }

    public function getWhere($array)
    {
        $values = $this->confirmKeys($array);

        $condition = '';

        foreach ($values as $key => $value) {
            $condition .= $key . " = :" . $key . " AND ";
            $params[':' . $key] = $value;
        }
        $condition = UTF8::substr($condition, 0, -4);

        $sql = "SELECT * FROM " . $this->_table_name . " WHERE " . $condition;

        return DB::query(1, $sql)->parameters($params)->execute();
    }

    public function whereFieldsBilder($query, $fields, $and = 0)
    {

        if ($and) {

            foreach ($fields as $key => $value) {
                $query->and_where($key, '=', $value);
            }
        } else {

            $f = TRUE;

            foreach ($fields as $key => $value) {

                if ($f) {
                    $query->where($key, '=', $value);
                } else {
                    $query->and_where($key, '=', $value);
                }
            }

        }

        return $query;
    }

    public function existRow($fields)
    {
        $values = $this->confirmKeys($fields);
        $keys = array_keys($values);

        $query = DB::select()->from(array($this->_table_name, 'a'));
        $query = $this->whereFieldsBilder($query, $fields);

        return count($query->execute()->as_array());
    }

    public function save($fields)
    {
        if (!isset($fields['id']) && $this->_table_name != 'relation') {
            return 'Идентификатора нет';
        }

        $values = $this->confirmKeys($fields);
        $keys = array_keys($values);

        if (!empty($fields['id'])) { //Вставка или редактирование
            $result = DB::update($this->_table_name)->set($values)->where('id',
                '=', $fields['id'])->execute();
        } else {
            $result = DB::insert($this->_table_name, $keys)->values($values)->execute();
        }

        return $result;
    }

    public function delete($id)
    {
        return DB::delete($this->_table_name)->where('id', '=', ':id')->param(':id',
            $id)->execute();
    }

    public function delWhere($fields)
    {
        $values = $this->confirmKeys($fields);

        $query = DB::delete($this->_table_name);
        $query = $this->andWhereArr($query, $values, 0);

        return $query->execute();
    }

    public function lastId()
    {
        return DB::select(array(DB::expr("LAST_INSERT_ID()"), "ID"))->execute()->get('ID');
    }

    protected function confirmKeys($fields)
    {
        $haystack = array_keys($this->_table_columns);
        $valueKeys = array_keys($fields);

        $values = array();

        foreach ($valueKeys as $key) {

            if (in_array($key, $haystack)) {
                $values[$key] = $fields[$key];
            }
        }
        if (!count($values)) return array(0);
        else return $values;
    }

    public function validation($array)
    {
        $validation = Validation::factory($array);
        $_rules = $this->rules();

        foreach ($_rules as $field => $rules) {
            foreach ($rules as $rule) {

                $validation->rule($field, $rule[0], Arr::get($rule, 1));
            }
        }

        return $validation;
    }

}