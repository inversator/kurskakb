<?php

/**
 * Created by PhpStorm.
 * User: maa
 * Date: 16.03.2017
 * Time: 14:18
 */
class Helper_Category
{
    public static $level = 0;

    public static function adminRecChild(array $catList, $parCatList, $html = '', $page, $down = 0)
    {
        foreach ($catList as $item) {
            $html .= '<tr>';
            $html .= '<td>';


            if (self::$level) {
                $counter = self::$level;
                while ($counter--) $html .= '<span style="padding-left: 30px"></span>';
                $html .= '└-  ';
            }

            $html .= $item['pTitle'] . '</td>' .
                '<td>' . $item['url'] . '</td>' .
                '<td><input ';

            if (in_array($item['id'], $parCatList)) {
                $html .= "checked='checked' ";
            }

            $html .= 'class="parentCheck' . $item['id'] . '-category" type="checkbox"' .
                'onchange="parentRec(' . $item['id'] . ", " . $page['id'] .
                ", 'category', 'unit');" .
                '"/></td>';

            $html .= '</tr>';

            if (count($item['child'])) $html = self::adminRecChild($item['child'], $parCatList, $html, $page, self::$level++);

        }
        self::$level--;
        return $html;
    }

    public static function adminRecChildSelf(array $catList, $parCatList, $html = '', $page, $down = 0)
    {
        foreach ($catList as $item) {
            $html .= '<tr>';
            $html .= '<td>';


            if (self::$level) {
                $counter = self::$level;
                while ($counter--) $html .= '<span style="padding-left: 30px"></span>';
                $html .= '└-  ';
            }

            $html .= $item['pTitle'] . '</td>' .
                '<td>' . $item['url'] . '</td>' .
                '<td>';
            if($page['id'] != $item['id']) {

                $html .= '<input ';

                if (in_array($item['id'], $parCatList)) {
                    $html .= "checked='checked' ";
                }

                $html .= 'class="parentCheck' . $item['id'] . '-category" type="checkbox"' .
                    'onchange="parentRec(' . $item['id'] . ", " . $page['id'] .
                    ", 'category', 'category');" .
                    '"/>';
            } else {
                $html .= '<span class="label label-info">текущая категория</span>';
            }

            $html .= '</td></tr>';

            if (count($item['child'])) $html = self::adminRecChildSelf($item['child'], $parCatList, $html, $page, self::$level++);

        }
        self::$level--;
        return $html;
    }

    public static function adminRecChildList(array $pages, $listNum, $html = '', $down = 0)
    {

        $num = 0;
        foreach ($pages as $page) {
            $num++;
            $html .= "<tr id='ItemDel-" . $page['id'] . "'>";

            $html .= "<td>";
            if (self::$level) {
                $counter = self::$level;
                while ($counter--) $html .= '<span style="padding-left: 30px"></span>';
                $html .= '└-  ';
            }

            $html .= "<a href='/admin/accumcat/edit/" . $page['id'] . "'>" . $page['pTitle'] . "</a></td>";
            $html .= '<td><button class = "btn btn-primary" type="button" onclick="location.href' .
                "='\/admin/accumcat/edit/" . $page['id'] . "';" .
                'return true;">Редактировать</button>';
            $html .= '</td>';
            $html .= '<td><button class="btn btn-danger" onclick="DelObjectAjax' .
                "('" . $page['id'] . "', '/admin/accumcat/delete/', 'ItemDel-" . $page['id'] . "')" .
                '; return true;" type="button">Удалить</button></td>';
            $html .= '</tr>';

            if (count($page['child'])) $html = self::adminRecChildList($page['child'], $listNum, $html, self::$level++);
        }

        self::$level--;
        return $html;
    }
}