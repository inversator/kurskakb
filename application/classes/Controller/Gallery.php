<?php

/**
 * Created by PhpStorm.
 * User: maa
 * Date: 09.03.2017
 * Time: 10:33
 */
class Controller_Gallery extends Controller_Base
{
    public $alias = '��������';
    public $essence = 'category';
    public $gender = 'female';
    public $model = 'Gallery';

    public function action_index()
    {
        if (!$slug = $this->request->param('slug2')) {
            $slug = $this->request->param('slug');
        }

        $gallery = Model::factory($this->model)->getOnUrl($slug);

        $content = View::factory('gallery');
        $content->self = $gallery;

        $this->template->bTitle = $gallery['bTitle'];
        $this->template->content = $content;
    }

    public function action_item()
    {
        if (!$slug = $this->request->param('slug2')) {
            $slug = $this->request->param('slug');
        }

        $gallery = Model::factory($this->model)->getOnUrl($slug);

        $content = View::factory('gallery');
        $content->self = $gallery;

        $images = Model::factory($this->model)->getImages($gallery['id']);
        $content->images = $images->as_array();

        $this->template->bTitle = $gallery['bTitle'];
        $this->template->content = $content;
    }
}