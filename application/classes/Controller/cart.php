<?php

class Controller_Cart extends Controller_Base
{
    public $alias = 'Корзина';
    public $essence = 'cart';
    public $gender = 'female';
    public $model = 'Cart';

    public function action_index()
    {
        $content = View::factory($this->essence);

        $content->title = 'Корзина';

        $sessionCart = session::instance()->get('cart');

        $cartBlock = Model::factory('cart')->getCart()->as_array('id');

        // Объединяю сессионные данные с данными модели
        $sum = 0;

        if (count($cartBlock)) {
            foreach ($cartBlock as $unit) {
                $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
            }

            foreach ($cartBlock as $unit) {
                $sum += $unit['value'] * $unit['count'];
            }
        }

        $content->cart = $cartBlock;
        $content->sum = $sum;


        $this->template->content = $content;
        $this->template->bTitle = $content->title;
        $this->template->content->rigthMenu = view::factory('block/rigth_menu');

        // Получаем список категорий 
        $categories = Model::factory('Category')->getList();
        $this->template->content->rigthMenu->list = $categories;
    }

    public function action_addToCart()
    {
        $this->auto_render = FALSE;
        $id = $this->request->post('id');
        $count = $this->request->post('post');
        return Model::factory($this->model)->addToCart($id, $count);
    }
    
    public function action_clearCart()
    {
        $this->auto_render = FALSE;
        return Model::factory($this->model)->clearCart();
    }

    public function action_updateCart()
    {
        $this->auto_render = FALSE;

        // Получаем данные для корзины
        $sessionCart = session::instance()->get('cart');

        $cartBlock = Model::factory('cart')->getCart()->as_array('id');

        $html = array();

        if (count($cartBlock)) {
            foreach ($cartBlock as $unit) {
                $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
            }

            $sum = 0;
            foreach ($cartBlock as $unit) {
                $sum += $unit['value'] * $unit['count'];
            }

            $cart = 'Сумма: <span class="badge">'.$sum.' ք</span>';
            $cart .= '<span class="cartBlock"> Товаров в корзине: '.count($cartBlock).'</span> ';
            $cart .= '<span class="glyphicon glyphicon-shopping-cart"></span>';

            $block = "<table class='table'>";
            foreach ($cartBlock as $unit):

                $block .= '<tr>'
                    .'<td><span class="glyphicon glyphicon-trash delFromCart" onclick="delFromCart('.$unit['id'].'); return false;"></span></td>'
                    .'<td>'.$unit['pTitle'].'<span class="badge">x'.$unit['count'].'</span></td>'
                    .'<td>'.$unit['value'] * $unit['count'].'</td>'
                    .'</tr>';

            endforeach;
            $block .= "<tr><td></td><td>Итого</td><td>$sum</td></tr>";
            $block .= "</table>";
            $block .= '<a href="/cart"><button style="width: 100%" class="btn btn-primary" type="button">Перейти в корзину</button></a>';
        } else {
            $cart = '<span class="cartBlock">Корзина пуста</span> ';
            $cart .= '<span class="glyphicon glyphicon-shopping-cart"></span>';
            $block = '<p>Вы не выбрали ни одного товара</p>';
        }
        $html['cart'] = $cart;
        $html['block'] = $block;

        $this->response->body(json_encode($html));
    }

    public function action_updateMainCart()
    {
        $this->auto_render = FALSE;

        // Получаем данные для корзины
        $sessionCart = session::instance()->get('cart');

        $cartBlock = Model::factory('cart')->getCart()->as_array('id');

        $html = array();

        if (count($cartBlock)) {
            foreach ($cartBlock as $unit) {
                $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
            }

            $sum = 0;
            foreach ($cartBlock as $unit) {
                $sum += $unit['value'] * $unit['count'];
            }

            $block = "";
            foreach ($cartBlock as $unit):

                $block .= '<tr>'
                    .'<td><span class="glyphicon glyphicon-trash delFromCart" onclick="delFromCart('.$unit['id'].'); update(); return false;"></span></td>'
                    .'<td><a href="/'.$unit['url'].'">'.$unit['pTitle'].'</a>'
                    .'<span class="right">
                                <span class="badge pointer" onclick="changeCountCart('.$unit['id'].', -1); update(); return false;">-</span>
                                <span class="badge">x
                                    '.$unit['count'].'
                                </span>
                                <span class="badge pointer" onclick="changeCountCart('.$unit['id'].', 1); update(); return false;">+</span>
                                </span>'
                    .'</td>'
                    .'<td>'.$unit['value'] * $unit['count'].'</td>'
                    .'</tr>';

            endforeach;
            $block .= "<tr><td></td><td>Итого</td><td>$sum</td></tr>";
        } else {
            $block = '<tr><td>Вы не выбрали ни одного товара</td></tr>';
        }
        $html = $block;

        $this->response->body($html);
    }

    public function action_delFromCart()
    {
        $this->auto_render = FALSE;

        Model::factory('cart')->delFromCart($this->request->query('id'));
    }

    public function action_changeCount()
    {
        $this->auto_render = FALSE;

        $id = $this->request->query('id');
        $sign = $this->request->query('sign');

        Model::factory('cart')->changeCount($id, $sign);
    }
}