<?php

/**
 * Created by PhpStorm.
 * User: maa
 * Date: 23.03.2017
 * Time: 15:31
 */
class Controller_File extends Controller
{
    public function action_get()
    {
        $essence = Arr::get($_GET, 'essence');
        $id_es = Arr::get($_GET, 'id_es');

        $docsRes = ORM::factory('File')->where('id_es', '=', $id_es)->where('essence', '=', $essence)->find_all();

        foreach ($docsRes as $doc) {
            $docs[] = $doc->as_array();
        }

        $this->response->body(json_encode($docs));
    }

    public function action_desc()
    {
        $this->response->body(Arr::get($_GET, 'id_file'));
        $idFile = Arr::get($_GET, 'id_file');
        $descFile = Arr::get($_GET, 'desc_file');

        $file = new Model_File();
        $file->desc($idFile, $descFile);
    }
}