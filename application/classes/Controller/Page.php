<?php

class Controller_Page extends Controller_Base
{

    public function action_index()
    {
        $page = Model::factory('Page')->get(1);
        $content = View::factory('face');

        $content->title = $page['pTitle'];
        $content->desc = $page['desc'];
        $content->text = $page['text'];

        $this->template->content = $content;
        $this->template->bTitle = $page['bTitle'];

        // Выводим продукты
        $starts = Model::factory('Category')->takeUnits(41)->as_array();
        $indastrial = Model::factory('Category')->takeUnits(42)->as_array();

        array_walk($indastrial, function(&$value, $key, $id){
            $value['image'] = '300x300.png';
            $value['class'] = 'indastrialFace';
        }, 42);

        $this->template->content->prodList = array_merge($starts, $indastrial);

        // Выводим новости
        $news = Model::factory('Category')->takeUnits(39);
        $this->template->content->newsList = $news->as_array('id');
    }

    public function action_item()
    {
        if($this->request->param('slug2')) {
            $slug = $this->request->param('slug2');
        } else {
            $slug = $this->request->param('slug');
        }

        if ($slug == 'main') $this->redirect('/');
        $page = Model::factory('Page')->getOnUrl($slug);

        $content = View::factory('page');

        $content->title = $page['pTitle'];
        $content->desc = htmlspecialchars_decode($page['desc']);
        $content->text = htmlspecialchars_decode($page['text']);
        $this->template->content = $content;
        $this->template->bTitle = $page['bTitle'];
        
    }
    public function action_dealers()
    {
        $content = View::factory('page/dealers');
        $this->template->content = $content;
    }

    public function action_faq()
    {
        $model = Model::factory('Question');

        $count = $model->count();

        $asincPag = new asincPag;
        $pagination = $asincPag->create($count, $this->request->query('page'), 20);

        // get question list
        $questions = $model->takeWithPag($asincPag->itemsPerPage, $asincPag->offset);

        $content = View::factory('page/faq');
        $content->questions = $questions->as_array();
        $content->pagination = $pagination;

        $this->template->content = $content;
    }
    public function action_contacts()
    {
        $content = View::factory('page/contacts');
        $this->template->content = $content;
    }
    public function action_404()
    {

    }

    public function action_filters()
    {

        $slug = $this->request->post('slug');
        $page = $this->request->post('page');

        $model = Model::factory($slug);
        $count = $model->count();
        $asincPag = new asincPag;

        $pagination = $asincPag->create($count, $page, 20);


        // get question list
        $units = $model->takeWithPag($asincPag->itemsPerPage, $asincPag->offset);

        $view = View::factory('pager_faq');

        $view->questions = $units->as_array();

        $arr['units'] = $view->render();
        $arr['pagination'] = $pagination;
        $this->response->body(json_encode($arr));

        $this->auto_render = FALSE;
    }
}