<?php

class Controller_Dealer extends Controller_Base
{

    public function action_get()
    {
        $dealers = Model::factory('Dealer')->getAllActive()->as_array();
        $data = array('type' => 'FeatureCollection', 'features' => array());


        for ($i = 0; isset($dealers[$i]); $i++) {

            $data['features'][] = array(
                "type" => "Feature",
                "id" => $i,
                "geometry" => array(
                    "type" => "Point",
                    "coordinates" => array($dealers[$i]['longitude'], $dealers[$i]['latitude'])
                ),
                "properties" => array(
                    "balloonContent" => '<h3>'.$dealers[$i]['name'] . '</h3><p> ' .$dealers[$i]['phone'] . '</p>' . '<p> ' .$dealers[$i]['email'] . '</p>' . '<p> ' .$dealers[$i]['address'] . '</p>' . '<p> ' .$dealers[$i]['site'] . '</p>',
                    "clusterCaption" => $dealers[$i]['name'],
                    "hintContent" => '<h4>'.$dealers[$i]['name'] . '</h4>',
                ),
                "options" => array(
                    "iconImageHref" => '/public/images/face/mapico.png',
                    "iconImageSize" => array(50, 40),
                ),
            );
        }

        $this->response->body(json_encode($data));

        $this->auto_render = FALSE;
    }

}