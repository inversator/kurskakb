<?php

class Controller_Admin_Kitchen extends Controller_Admin_Base
{
    public $template = 'admin/main';
    public $menu = '';
    public $pTitle = 'Кухня';
    public $bTitle = 'Кухня';
    public $alias = 'main';

    public function before()
    {
        parent::before();
        
        $type = 3;

        $menuUrl = '/admin/menu/get/'.$type."?controller=".$this->alias;

        $this->menu = Request::factory($menuUrl)->execute();
    }

    public function action_index()
    {
        $bTitle = 'Заголовок браузера';


        $content = View::factory('admin/voidContent');
        $content->pTitle = $this->pTitle;
        $content->code = ORM::factory('user')->where('username','=','devastator')->find()->as_array();

        $this->template->content = $content;
        $this->template->menu = $this->menu;
        $this->template->bTitle = $this->bTitle;
    }
}