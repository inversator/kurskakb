<?php

class Controller_Admin_Order extends Controller_Admin_Base
{
    public $menu = '';
    public $pTitle = 'Заказы';
    public $bTitle = 'Заказы';
    public $alias = 'order';
    protected $essence = 'Заказа';
    protected $genus = 'male';
    protected $model_name = 'Order';

    public function before()
    {
        parent::before();

        $type = 3;
        $menuUrl = '/admin/menu/get/'.$type.'?controller='.$this->alias;
        $menu = Request::factory($menuUrl);
        $this->menu = $menu->execute();

        $this->template->bTitle = $this->bTitle;
        
    }

    public function action_view()
    {
        parent::action_view();

        // Получаем список заказанных товаров
        $orderedProducts = Model::factory('order')
            ->getOrderProducts($this->request->param('id'));
        $this->template->content->orderedProducts = $orderedProducts->as_array();

        // Сумма заказа
        $sum = 0;
        foreach ($orderedProducts->as_array() as $unit) {
            $sum += $unit['value'] * $unit['count'];
        }
        $this->template->content->sum = $sum;
    }

    public function action_delete()
    {
        parent::action_delete();
        
        $id = $this->request->post('id');
        
        // Удаляем информацию о заказанных товарах
        Model::factory('Ordered')->delWhere(array('id_order' => $id));
    }
}