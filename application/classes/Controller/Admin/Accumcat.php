<?php
defined('SYSPATH') OR die('Прямой доступ запрещен');

class Controller_Admin_Accumcat extends Controller_Admin_Category
{
    public $menu = '';
    public $pTitle = 'Категории';
    public $bTitle = 'Категории';
    public $alias = 'accumcat';
    protected $essence = 'Категория';
    protected $genus = 'female';
    protected $model_name = 'category';

    public $catId = 40;

    public function before()
    {
        parent::before();

        $type = 3;
        $menuUrl = '/admin/menu/get/' . $type . '?controller=' . $this->alias;
        $menu = Request::factory($menuUrl);
        $this->menu = $menu->execute();

        $this->template->bTitle = $this->bTitle;
    }

    public function action_index()
    {
        $pages = array();

        $model = Model::factory(ucfirst($this->model_name));

        $count = $model->countWhereParent('category', $this->catId);

        $pagination = Pagination::factory(array(
                'total_items' => $count,
            )
        )->route_params(array(
            'directory' => strtolower($this->request->directory()),
            'controller' => strtolower($this->request->controller()),
            'action' => 'list',
        ));

//        $pages = $model->getParLim(
//            array('category', $this->catId),
//            array($pagination->offset, $pagination->items_per_page));

        // Получаем массив категорий каталога
        $pages = Model::factory('Category')->getChildBranch('category', 'category', $this->catId);

        $content = View::factory('admin/' . $this->alias . '/list');

        $content->pTitle = $this->pTitle;
        $content->pages = $pages;
        $content->pagination = $pagination;
        $content->alias = $this->alias;

        $this->template->content = $content;
        $this->template->menu = $this->menu;

        // Получаем номер страницы
        $listNum = $this->request->param('id') ? $this->request->param('id') : 1;
        $this->template->content->listNum = $listNum;
    }

    public function action_edit()
    {
        parent::action_edit();

        // Получаем изображения
        $modelImage = Model::factory(ucfirst('image'));
        $id = $this->request->param('id');

        $images = $modelImage->getImage('category', $id);
        $this->template->content->images = $images;

        // Получаем список категорий
        $catList = Model::factory('Category')->getChildBranch('category', 'category', $this->catId);
        $this->template->content->catList = $catList;

        // Получаем список родительских категорий
        $relModel = Model::factory('Relation');

        $relResult = $relModel->getWhere(array('dot_record' => $id, 'dot_essence' => 'category'))->as_array();

        $parList = array();

        foreach ($relResult as $relation) {

            if (isset($relation['par_record'])) $parList[] = $relation['par_record'];
        }

        $this->template->content->parCatList = $parList;
    }

    public function action_delete()
    {
        $id = Arr::get($_POST, 'id', 0);

        // Удаляем связи с изображениями и изображения
        $images = Model::factory(ucfirst('image'))->getImage('category', $id);
        foreach ($images as $image) {
            unlink('./public/images/' . $image['essence'] . '/' . $image['name']);
        }
        Model::factory(ucfirst('image'))->delWhere(array('id_es' => $id, 'essence' => 'category'));

        //Удаляем связи
        Model::factory(ucfirst('Relation'))->unlink($id, $this->alias);

        parent::action_delete();
    }

    public function action_save()
    {
        $this->auto_render = FALSE;

        $message = '';

        if (!isset($_POST['id'])) {
            $message = 'Отсутствует идентификатор';
        } else {

            $request = Form::pre($_POST);

            $model = Model_Base::factory(ucfirst($this->model_name), $request['id']);

            $post = $model->validation($request);

            if ($post->check()) {

                if ($model->save($request)) {
                    if (!empty($request['id'])) {
                        $message = 'Изменения сохранены';
                    } else {
                        $message = $this->essence . ' добавлен' . KOHANA::$config->load('converter.genus.' . $this->genus) . '. ';
                        $message .= '<a href="/admin/' . $this->alias . '/edit/' . $model->lastId() . '">Перейти к редактированию -></a>';
                        Request::factory('/admin/category/addParent?par_rec=' . $this->catId . '&dot_rec=' . $model->lastId() . '&par_ess=category&dot_ess=category')->execute();
                    }
                } else {
                    $message = 'Вы не изменили данные';
                };
            } else {
                foreach ($post->errors('comments') as $error) {
                    $message .= "<p>" . $error . "</p>";
                }
            }
        }

        $this->response->body($message);
    }
}