<?php
defined('SYSPATH') OR die('Прямой доступ запрещен');

class Controller_Admin_Page extends Controller_Admin_Base
{
    public $menu = '';
    public $pTitle = 'Страницы';
    public $bTitle = 'Страницы';
    public $alias = 'page';
    protected $essence = 'Страница';
    public $genus = 'female';
    protected $model_name = 'page';

    public function before()
    {
        parent::before();

        $type = 3;

        $menuUrl = '/admin/menu/get/'.$type.'?controller='.$this->alias;
        $controller = 'page2';
        $menu = Request::factory($menuUrl, $controller);
        $this->menu = $menu->execute();

        $this->template->bTitle = $this->bTitle;
    }

    public function action_edit()
    {
        parent::action_edit();

        $id = $this->request->param('id');

        // Получаем список страниц
        $list = Model::factory('page')->getAll();
        $this->template->content->list = $list;

        // Получаем список родительских страниц
        $relModel = Model::factory('Relation');
        $relResult = $relModel->getWhere(array('dot_record' => $id, 'dot_essence' => 'page'));
        $parList = array();

        foreach ($relResult as $relation) {
            $parList[] = $relation['par_record'];
        }

        $this->template->content->parList = $parList;
    }
}