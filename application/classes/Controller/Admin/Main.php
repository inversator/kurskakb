<?php

class Controller_Admin_Main extends Controller_Admin_Base
{
    public $template = 'admin/main';
    public $menu = '';
    public $pTitle = 'Отчет по сайту';
    public $bTitle = 'Отчет по сайту';
    public $alias = 'main';

    public function before()
    {
        parent::before();
        
        $type = 3;

        $menuUrl = '/admin/menu/get/'.$type."?controller=".$this->alias;

        $this->menu = Request::factory($menuUrl)->execute();
    }

    public function action_index()
    {
        $bTitle = 'Заголовок браузера';


        $content = View::factory('admin/content');
        $content->pTitle = $this->pTitle;


        $this->template->content = $content;
        $this->template->menu = $this->menu;
        $this->template->bTitle = $this->bTitle;
    }
}