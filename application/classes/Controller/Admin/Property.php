<?php
defined('SYSPATH') OR die('Прямой доступ запрещен');

class Controller_Admin_Property extends Controller_Admin_Base
{
    public $menu = '';
    public $pTitle = 'Свойства';
    public $bTitle = 'Свойства';
    public $alias = 'property';
    protected $essence = 'Свойство';
    protected $genus = 'average';
    protected $model_name = 'Property';

    public function before()
    {
        parent::before();

        $type = 3;
        $menuUrl = '/admin/menu/get/'.$type.'?controller='.$this->alias;
        $menu = Request::factory($menuUrl);
        $this->menu = $menu->execute();

        $this->template->bTitle = $this->bTitle;
    }

    public function action_edit()
    {
        parent::action_edit();

        // Получаем значения свойства
        $id = $this->request->param('id');
        $fixedValues = Model::factory('Fixedprop')->getWhere(array('id_prop' => $id));

        $this->template->content->items = $fixedValues;
    }

    public function action_addValue()
    {
        $this->auto_render = FALSE;
        $message = '';
        $post = $this->request->post();

        if ($post['value']) {

            if ($post['fixed']) {
                if (Model::factory('Fixedprop')->add($post)) {
                    $message = 'Значение добавлено';
                } else {
                    $message = 'Не удалось добавить значение';
                };
            } else {
                return Model::factory('freeprop')->add($post);
            }
        } else {
            $message = 'Введите значение';
        }

        $this->response->body($message);
    }

    public function action_delFixedValue()
    {
        $this->auto_render = FALSE;
        $message = '';
        $id = $this->request->post('id');

        if (Model::factory('Fixedprop')->delete($id)) {
            $message = 'Значение удалено';
        } else {
            $message = 'Не удалось удалить значение';
        }

        $this->response->body($message);
    }

    public function action_getFixedValues()
    {
        $this->auto_render = FALSE;

        $id_prop = ARR::GET($_GET, 'id_prop', 0);
        $fields = array('id_prop' => $id_prop);

        $properties = Model::factory('Fixedprop')->getWhere($fields);

        $this->response->body(json_encode($properties->as_array()));
    }

    public function action_getType()
    {
        $this->auto_render = FALSE;
        $id = ARR::GET($_GET, 'id', 0);

        $type = Model::factory($this->model_name)
            ->getOneColumn($id, 'fixed')
            ->as_array();
        count($type) ? $this->response->body($type[0]['fixed']) : '';
    }

    public function action_delete()
    {
        $id_prop = Arr::get($_POST, 'id', 0);

        Model::factory(ucfirst('Fixedprop'))->delWhere(
            array('id_prop' => $id_prop)
        );

        Model::factory(ucfirst('Propvalue'))->delWhere(
            array('id_prop' => $id_prop)
        );

        parent::action_delete();
    }
}