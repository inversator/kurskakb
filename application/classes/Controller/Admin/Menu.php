<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Controller_Admin_Menu extends Controller_Admin_Base
{
    public $menu = '';
    
    public $pTitle = 'Меню';
    public $bTitle = 'Меню';
    public $alias = 'menu';
    
    protected $essence = 'Меню';
    protected $genus = 'average';
    protected $model_name = 'menu';

    public function before()
    {

        if (Request::initial() === Request::current()) {

            parent::before();
            $type = 3;

            $menuUrl = '/admin/menu/get/'.$type.'?controller='.$this->alias;

            $menu = Request::factory($menuUrl);

            $this->menu = $menu->execute();

            $this->template->bTitle = $this->bTitle;
        } else {
            $this->auto_render = FALSE;
        }
    }

    public function action_index()
    {
        $pages = array();

        $model = new Model_Menu();
        $pages = $model->getAll();

        $content = View::factory('admin/menu/list');

        $content->pTitle = $this->pTitle;
        $content->pages = $pages;

        $this->template->content = $content;
        $this->template->menu = $this->menu;
    }

    public function action_items()
    {
        $items = array();

        $type = $this->request->param('id');

        $model = new Model_MenuItem();
        $items = $model->getAll($type)->as_array();

        foreach($items as $k=>$item) {
            $items[$k]['child'] = $model->getSubItems($item['id']);
        }

        $pages = Model::factory('page')->getAll();

        $content = View::factory('admin/menu/items');

        $content->menu = Model::factory('Menu')->get($type);


        $content->type = $type;
        $content->pages = $pages;
        $content->pTitle = $this->pTitle;
        $content->items = $items;

        $this->template->content = $content;
        $this->template->menu = $this->menu;
    }

    public function delItem()
    {
        $this->auto_render = FALSE;

        $id = $this->request->param('id');

        if (!$id) {
            $this->response->body('Не передан ID');
        }

        $model = Model::factory('menuItem');
        return $model->delete($id);
    }

    public function action_edit()
    {

        $id = $this->request->param('id');

        if (!$id) {
            $this->redirect('/admin/menu/');
        }

        $content = View::factory('admin/menu/edit');

        $content->pTitle = 'Редактирование';

        $page = array();
        $model = new Model_Menu();

        $page = $model->get($id);
        $content->page = $page;

        $this->template->content = $content;
        $this->template->content ->act = 'edit';
        $this->template->menu = $this->menu;
    }

    public function action_new()
    {

        $content = View::factory('admin/menu/edit');

        $content->pTitle = 'Создание';
        $content->page = array();

        $this->template->content = $content;
        $this->template->menu = $this->menu;
    }

    public function action_addItem()
    {
        $this->auto_render = FALSE;
        $message = '';
        $fields = array();

        if (!count($_POST)) {
            $message = 'Данные не поступили';
        } else {

            $page = Model::factory('page')->get($_POST['page']);

            $fields['type'] = $_POST['type'];

            $fields['alias'] = $page['pTitle'];
            $fields['name'] = $page['url'];


            $post = Validation::factory($fields);

            $post->rule('type', 'not_empty')
                ->rule('type', 'numeric');

            if ($post->check()) {

                $model = Model::factory('menuItem');
                $replica = $model->getWhere(array('name' => $fields['name'], 'type' => $fields['type']));
                if (!count($replica[0])) {
                    if ($model->add($fields)) {
                        $message = 'Пункт добавлен к меню';
                    } else {
                        $message = 'Неизвестная ошибка';
                    }
                } else {
                    $message = 'Пункт с заданным alias уже существует';
                }
            } else {
                foreach ($post->errors('comments') as $error) {
                    $message .= "<p>".$error."</p>";
                }
            }
        }

        $this->response->body($message);
    }

    public function action_addSubItem()
    {
        $this->auto_render = FALSE;
        $message = '';
        $fields = array();

        if (!count($_POST)) {
            $message = 'Данные не поступили';
        } else {

            $page = Model::factory('page')->get($_POST['page']);

            $fields['par_item'] = $_POST['type'];

            $fields['alias'] = $page['pTitle'];
            $fields['name'] = $page['url'];


            $post = Validation::factory($fields);

            $post->rule('par_item', 'not_empty')
                ->rule('par_item', 'numeric');

            if ($post->check()) {

                $model = Model::factory('menuItem');
                $replica = $model->getWhere(array('name' => $fields['name'], 'par_item' => $fields['par_item']));
                if (!count($replica[0])) {
                    if ($model->add($fields)) {
                        $message = 'Пункт добавлен к меню';
                    } else {
                        $message = 'Неизвестная ошибка';
                    }
                } else {
                    Room::die_dump($replica);
                    $message = 'Пункт с заданным alias уже существует';
                }
            } else {
                foreach ($post->errors('comments') as $error) {
                    $message .= "<p>".$error."</p>";
                }
            }
        }

        $this->response->body($message);
    }


    public function action_delItem()
    {
        $this->auto_render = FALSE;
        $message = '';

        if (!count($_POST)) {
            $message = 'Данные не поступили';
        } else {
            $model = Model::factory('menuItem');

            if ($model->delete(Arr::get($_POST, 'id', 0))) {
                $message = 'Пункт меню удален';
            } else {
                $message = 'Данного пункта не существует';
            }
        }
        $this->response->body($message);
    }

    public function action_save()
    {

        $this->auto_render = FALSE;

        $message = '';

        if (!isset($_POST['id'])) {
            $message = 'Отсутствует идентификатор';
        } else {

            if (!isset($_POST['off'])) {

                $_POST['off'] = 1;
            } else {
                $_POST['off'] = 0;
            }

            $post = Validation::factory($_POST);
            $post->rule('name', 'not_empty')
                ->rule('name', 'max_length', array(':value', 225))
                ->rule('alias', 'max_length', array(':value', 225))
                ->rule('comment', 'max_length', array(':value', 225))
            ;

            if ($post->check()) {
                $model = Model::factory($this->alias);

                if ($model->save($_POST)) {
                    if (!empty($_POST['id'])) $message = 'Изменения сохранены';
                    else $message = 'Меню добавлено';
                } else {
                    $message = 'Вы не изменили данные';
                };
            } else {
                foreach ($post->errors('comments') as $error) {
                    $message .= "<p>".$error."</p>";
                }
            }
        }

        $this->response->body($message);
    }

    public function action_get()
    {
        $type = $this->request->param('id');

        $model = new Model_MenuItem();
        $items = $model->getActiveItems($type);

        $menu = View::factory('admin/blocks/menu');
        $menu->lis = '';

        foreach ($items as $item) {

            $user = Auth::instance()->get_user();
            $managers = $user->roles->find_all();

            $roles = array();

            foreach ($managers as $manager) {
                $roles[] = $manager->id;
            }

            $accesses = explode(',', $item['access']);
            if(array_intersect($roles, $accesses)) $menu->lis .= Menu::liMark($item['name'], $item['alias']);
        }
        $this->response->body($menu);
    }
}