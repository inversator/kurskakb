<?php
defined('SYSPATH') OR die('Прямой доступ запрещен');

class Controller_Admin_Gallery extends Controller_Admin_Base
{
    public $menu = '';
    public $pTitle = 'Галлереи';
    public $bTitle = 'Галлереи';
    public $alias = 'gallery';
    protected $essence = 'Галлерея';
    protected $genus = 'female';
    protected $model_name = 'gallery';

    public function before()
    {
        parent::before();

        $type = 3;
        $menuUrl = '/admin/menu/get/'.$type.'?controller='.$this->alias;
        $menu = Request::factory($menuUrl);
        $this->menu = $menu->execute();

        $this->template->bTitle = $this->bTitle;
    }

    public function action_edit()
    {
        parent::action_edit();

        // Получаем изображения
        $modelImage = Model::factory(ucfirst('image'));
        $id = $this->request->param('id');

        $images = $modelImage->getImage($this->alias, $id);
        $this->template->content->images = $images;

        // Получаем список категорий
        $catList = Model::factory(ucfirst('category'))->getAll();
        $this->template->content->catList = $catList;

        // Получаем список родительских категорий
        $relModel = Model::factory(ucfirst('Relation'));
        $relResult = $relModel->getWhere(array('dot_record' => $id, 'dot_essence' => 'category'));
        $parList = array();

        foreach ($relResult as $relation) {
            if (isset($relation['par_cat'])) $parList[] = $relation['par_cat'];
        }

        $this->template->content->parList = $parList;
    }

    public function action_delete()
    {
        $id = Arr::get($_POST, 'id', 0);

        // Удаляем связи с изображениями и изображения
        $images = Model::factory(ucfirst('image'))->getImage($this->alias, $id);
        foreach ($images as $image) {
            unlink('./public/images/'.$image['essence'].'/'.$image['name']);
        }
        Model::factory('image')->delWhere(array('id_es' => $id, 'essence' => $this->alias));

        //Удаляем связи
        Model::factory('Relation')->unlink($id, $this->alias);

        parent::action_delete();
    }
}