<?php

/**
 * Created by PhpStorm.
 * User: maa
 * Date: 13.03.2017
 * Time: 16:26
 */
class Controller_Admin_News extends Controller_Admin_Unit
{
    public $catId = 39;
    public $pTitle = 'Новости';
    public $bTitle = 'Новости';
    protected $essence = 'Новость';
    protected $genus = 'female';
    protected $model_name = 'Unit';

    public function before()
    {
        parent::before();
        $type = 3;
        $menuUrl = '/admin/menu/get/' . $type . '?controller=news';
        $menu = Request::factory($menuUrl);
        $this->menu = $menu->execute();

        $this->template->bTitle = $this->bTitle;
    }

    public function action_index()
    {
        $pages = array();

        $this->alias = 'news';

        $model = Model::factory(ucfirst($this->model_name));

        $count = $model->countWhereParent('category', $this->catId);

        $pagination = Pagination::factory(array(
                'total_items' => $count,
            )
        )->route_params(array(
            'directory' => strtolower($this->request->directory()),
            'controller' => strtolower($this->request->controller()),
            'action' => 'list',
        ));

        $pages = $model->getParLim(
            array('category', $this->catId),
            array($pagination->offset, $pagination->items_per_page));

        $content = View::factory('admin/' . $this->alias . '/list');

        $content->pTitle = $this->pTitle;
        $content->pages = $pages;
        $content->pagination = $pagination;

        $this->template->content = $content;
        $this->template->menu = $this->menu;

        // Получаем номер страницы
        $listNum = $this->request->param('id') ? $this->request->param('id') : 1;
        $this->template->content->listNum = $listNum;
    }

    public function action_edit()
    {
        $this->alias = 'news';
        parent::action_edit();

        $this->template->content->list = Model::factory('Category')->takeUnits($this->catId);
    }

    public function action_save()
    {
        $this->alias = 'news';

        $this->auto_render = FALSE;

        $message = '';

        if (!isset($_POST['id'])) {
            $message = 'Отсутствует идентификатор';
        } else {

            $request = Form::pre($_POST);

            $model = Model_Base::factory(ucfirst('unit'), $request['id']);

            $post = $model->validation($request);

            if ($post->check()) {

                if ($model->save($request)) {
                    if (!empty($request['id'])) {
                        $message = 'Изменения сохранены';
                    } else {
                        $message = $this->essence . ' добавлен' . KOHANA::$config->load('converter.genus.' . $this->genus) . '. ';
                        $message .= '<a href="/admin/' . $this->alias . '/edit/' . $model->lastId() . '">Перейти к редактированию -></a>';
                        Request::factory('/admin/category/addParent?par_rec=' . $this->catId . '&dot_rec=' . $model->lastId() . '&par_ess=category&dot_ess=unit')->execute();
                    }
                } else {
                    $message = 'Вы не изменили данные';
                };
            } else {
                foreach ($post->errors('comments') as $error) {
                    $message .= "<p>" . $error . "</p>";
                }
            }
        }

        $this->response->body($message);
    }

    public function action_new()
    {
        $this->alias = 'news';
        parent::action_new();
    }

    public function action_delete()
    {
        $this->alias = 'news';
        parent::action_delete();
    }
}