<?php
defined('SYSPATH') OR die('Прямой доступ запрещен');

class Controller_Admin_Question extends Controller_Admin_Base
{
    public $menu = '';
    public $pTitle = 'Вопросы';
    public $bTitle = 'Вопросы';
    public $alias = 'question';
    protected $essence = 'Вопрос';
    protected $genus = 'male';
    protected $model_name = 'question';

    public function before()
    {
        parent::before();

        $type = 3;
        $menuUrl = '/admin/menu/get/'.$type.'?controller='.$this->alias;
        $menu = Request::factory($menuUrl);
        $this->menu = $menu->execute();

        $this->template->bTitle = $this->bTitle;
    }

}