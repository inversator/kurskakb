<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Users extends Controller_Admin_Base
{

    public function action_login()
    {
        if ($post = $this->request->post()) {
            
            $this->auto_render = FALSE;
            
            // Если значения логина и пароля не пустые то авторизируемся на сайте
            if (!empty($post['username']) && !empty($post['password'])) {
                Auth::instance()->login($post['username'], $post['password']);
            }
            
            if(Auth::instance()->logged_in()){
                $this->response->body(json_encode(array(1,'/admin/users/login')));
            } else {
                $this->response->body(json_encode(array(0,'Неверный логин, пароль или Вы')));
            }
            
            
        }
        // Проверяем авторизировался пользователь или нет
        if (Auth::instance()->logged_in()) {
            $this->template->content = View::factory('/admin/user_profile');
        } else {
            $this->template->content = View::factory('/admin/form_login');
        }
    }

    public function action_registration()
    {
        if ($post = $this->request->post()) {

            $this->auto_render = FALSE;

            $validation = Validation::factory($post);
            $validation->rule('username', 'max_length', array(':value', 25))
                ->rule('username', 'not_empty')
                ->rule('username', 'english_num')
                ->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('password', 'not_empty')
                ->rule('password', 'min_length', array(':value', 8))
                ->rule('password', 'max_length', array(':value', 20))
                ->rule('password_confirm', 'matches',
                    array(':validation', 'password_confirm', 'password'))
            ;

            $overlap_name = ORM::factory('user')->where('username', '=', $post['username'])->find()->as_array();
            $overlap_mail = ORM::factory('user')->where('email', '=', $post['email'])->find()->as_array();

            if ($validation->check() && !$overlap_name['id'] && !$overlap_mail['id']) {
                // Создаем хеш
                $token = md5(time().$_POST['username'].$_POST['email']);

                // Сохраняем пользователя в БД
                $data = array(
                    'username' => $_POST['username'],
                    'email' => $_POST['email'],
                    'password' => $_POST['password'],
                    'password_confirm' => $_POST['password_confirm'],
                    'token' => $token,
                );
                $user = ORM::factory('User')->create_user($data,
                    array('username', 'email', 'password', 'token'));

                // Если нужно подтверждать емаил то не нужно выставлять роль login, иначе он и без подтверждения E-mail сможет авторизоваться на сайте
                // $user->add('roles',ORM::factory('role',array('name'=>'login')));
                // Создаем ссылку для подтверждения E-mail
                $url = 'http://'.$_SERVER['HTTP_HOST'].'/users/approved?token='.$token;

                // Отправляем письмо пользователю с ссылкой для подтверждения E-mail
                mail($post['email'], 'Регистрация на сайте kohanы',
                    'Вы были зерегестрированы на сайте kohanы, для подтверждения E-mail пройдите по ссылке '.$url);
                $response = json_encode(array(1,'/users/login?submit=1'));
                $this->response->body($response);
                //$this->response->body('На указанный Вами email("'.$post['email'].'") было отправлено письмо. Пройдите по ссылке указанной в нем чтобы завершить регистрацию');
            } else {
                $messages = '';
                foreach ($validation->errors('comments') as $message) {
                    $messages .= "<p class='text-danger'>".$message."</p>";
                }

                if ($overlap_name['id']) {
                    $messages .= '<p>Логин "'.$post['username'].'" занят. Придумайте другой</p>';
                }
                if ($overlap_mail['id']) {
                    $messages .= '<p>Почта "'.$post['email'].'" уже тспользуется. Введите другой адрес</p>';
                }
                $response = json_encode(array(0,$messages));
                
                $this->response->body($response);
            }
        } else {

            // Выводим шаблон регистрации
            $this->template->content = View::factory('registration_form');
        }
    }
    
    public function action_approved()
    {
        $token = $this->request->query('token');
        if($token){
            // ищем пользователя с нужным токеном
            $user = ORM::factory('User')->where('token', '=', $token)->find();
            if($user->get('id')){
            
                // добавляем пользователю роль login, чтобы он мог авторизоваться
                $user->add('roles',ORM::factory('role',array('name'=>'login')));
                
                // Чистим поле с токеном
                $user->update_user(array('token'=>null), array('token'));
                
                // Можно сразу и авторизовать и перенаправить ЛК
                Auth::instance()->force_login($user->get('username'));
                $this->redirect("/users/login");
                
                // Или переадресовать на форму входа для ввода логина и пароль
                //$this->redirect("/users/login");
            }
        }
     
        // Делаем редирект на страницу авторизации
        $this->redirect("/users/login");
    }

    public function action_logout()
    {
        // Разлогиниваем пользователя
        Auth::instance()->logout();
        // Редиректим его на страницу авторизации
        $this->redirect('/users/login');
    }
}