<?php

class Controller_Unit extends Controller_Base
{
    public $alias = 'Товар';
    public $essence = 'unit';
    public $gender = 'male';
    public $model = 'unit';

    public function action_index()
    {
        $page = Model::factory($this->model)->get(1);
        $content = View::factory($this->essence . '/info');

        $content->title = $page['pTitle'];
        $content->desc = $page['desc'];
        $content->text = $page['text'];
        $content->date = $page['date'];

        $this->template->content = $content;
        $this->template->bTitle = $page['bTitle'];
        $this->template->content->rigthMenu = view::factory('block/rigth_menu');

        // Получаем список категорий 
        $categories = Model::factory('category')->getList();
        $this->template->content->rigthMenu->list = $categories;
    }

    public function action_item()
    {
        if (!$slug = $this->request->param('unit')) {
            if (!$slug = $this->request->param('slug')) {
                $slug = $this->request->param('slug2');
            }

            $page = Model::factory($this->model)->getOnUrl($slug);
            $cat = Model::factory('Unit')->getParent($page['id']);
            $this->redirect('/' . $cat[0]['url'] . '/' . $slug);
        }

        $page = Model::factory($this->model)->getOnUrl($slug);
        $modelCat = Model::factory('Category');

        $content = View::factory($this->essence . '/info');

        $content->title = $page['pTitle'];
        $content->desc = $page['desc'];
        $content->text = $page['text'];
        $content->date = $page['date'];
        $content->id = $page['id'];

        $content->cat = $modelCat->getListName();
        $content->cat['catalog'] = array('url'=> 'catalog', 'pTitle' => 'Продукция');

        $this->template->content = $content;
        $this->template->bTitle = $page['bTitle'];
        $this->template->content->rigthMenu = view::factory('block/cat_menu');

        // Получаем список категорий (для меню)
        $categories = $modelCat->getChildBranch('category', 'category', 40);
        $this->template->content->rigthMenu->list = $categories;

        // Получаем список свойств товара
        $properties = Model::factory($this->model)->getProperties($page['id']);
        $properties = $properties->as_array();

        // Вынимаем и удаляем цену
        $price = 0;

        for ($i = 0; $i < count($properties); $i++) {
            if ($properties[$i]['id_prop'] == 1) {
                $price = $properties[$i]['value'];
                unset($properties[$i]);
            }
        }
        $this->template->content->price = $price;
        $this->template->content->properties = $properties;

        // Изображения
        $images = Model::factory($this->model)->getImages($page['id']);


        // Вынимаем главное фото
        $mainImage = NULL;
        $imageArr = array();
        $order = 0;

        foreach ($images->as_array() as $image) {

            $obj = Image::factory('./public/images/unit/' . $image['name']);

            $imageArr[$order]['name'] = $image['name'];
            $imageArr[$order]['width'] = $obj->width;
            $imageArr[$order]['height'] = $obj->height;

            if ($image['main']) {
                $mainImage = $imageArr[$order];
            }

            $order++;
        }
        if (count($imageArr)) $this->template->content->mainImage = $mainImage ? $mainImage : $imageArr[0];
        $this->template->content->images = $imageArr;

        // Получаем связанные товары
        $units = Model::factory($this->model)->relationUnits($page['id']);
        $this->template->content->units = $units->as_array();
        $this->template->content->category = !empty($this->request->param('category')) ? $this->request->param('category') : '';

        // Receiving documents
        $docs = ORM::factory('File')->where('id_es', '=', $page['id'])->where('essence', '=', 'unit')->find_all();
        $this->template->content->docs = $docs;
    }
}