<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Controller_Image extends Controller
{
    public $model_name = 'images';

    public function action_get()
    {   
        $essence = Arr::get($_GET, 'essence');
        $id_es = Arr::get($_GET, 'id_es');

        $modelImage = Model::factory(ucfirst('image'));

        $images = $modelImage->getImage($essence, $id_es);

        $this->response->body(json_encode($images));
    }

    public function action_main()
    {
        $id_img = Arr::get($_GET, 'id_img');
        $id_es = Arr::get($_GET, 'id_es');

        $model = Model::factory('image');
        $model->makeMain($id_img,$id_es);
    }

    public function action_desc()
    {

        $this->response->body(Arr::get($_GET, 'id_img'));
        $idImg = Arr::get($_GET, 'id_img');
        $descImg = Arr::get($_GET, 'desc_img');

        $model = Model::factory('image');
        $model->insertDesc($idImg,$descImg);
    }


}