<?php


class Router
{

    public function routesList()
    {
        $list = Model::factory('Routes')->getAll();

        return $list;
    }

    public function actionList()
    {
        $list = array();
        $routes = $this->routesList();

        foreach ($routes as $router) {
            $actions = Model::factory(ucfirst($router['name']))->getColumn('url');

            $actionList = implode('|', $actions['url']);

            $list[$router['name']] = $actionList;
        }

        return $list;
    }

    public function actionUnitList()
    {
        $list = array();
        
        $categoriesUrl = Model::factory('Category')->getColumn('url');
        $list[0] = implode('|', $categoriesUrl['url']);

        $unitsUrl = Model::factory('Unit')->getColumn('url');
        $list[1] = implode('|', $unitsUrl['url']);
              
        return $list;
    }
}