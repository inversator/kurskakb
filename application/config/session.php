<?php defined('SYSPATH') OR die('No direct script access.');

return array(

	'cookie' => array(
		'encrypted' => FALSE,
	),
	'native' => array(
		'lifetime' => 43200,
	),

);
