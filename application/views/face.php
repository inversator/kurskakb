</div>
<div class="container">
    <div class="faceBanner">
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="leftMenu">
                    <p><a href="/catalog/auto"><span class="glyphicon glyphicon-chevron-down"></span> Стартерные АКБ</a></p>

                    <p><a href="/catalog/industrial"><span class="glyphicon glyphicon-chevron-down"></span> Промышленые АКБ</a></p>

                    <p class="pad00019"><a href="/article/kakuyu-vybrat-akb-i-kak-pravil-no-eto-sdelat">Подобрать батарею</a></p>

                    <p class="pad00019"><a href="/dealers">Где купить</a></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-9 fac"><img src="/public/images/face/banner-blue2.png" /></div>
        </div>
        <h2 class="clr3 h1 faceHprod">ПРОДУКЦИЯ</h2>
    </div>

    <div class="row row-offcanvas row-offcanvas-right">
        <!--        <div class="col-xs-5 zavodName">-->
        <!--            <div class="page-header jumbotron">-->
        <!---->
        <!--                <h2>--><?php //echo $title; ?><!--</h2>-->
        <!---->
        <!--                <p class="lead">--><?php //echo htmlspecialchars_decode($desc); ?><!--</p>-->
        <!---->
        <!--            </div>-->
        <!--            <div class="fullText">--><?php //echo htmlspecialchars_decode($text); ?><!--</div>-->
        <!---->
        <!---->
        <!--        </div>-->

        <!--        <div class="col-xs-7">-->
        <!---->
        <!--        </div>-->
    </div>

</div>
<div class="faceProd">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 face">

                <!-- KAZ production -->
                <?php
                if (isset($prodList) && count($prodList)): ?>
                <?php if (count($prodList) > 3): ?>
                    <div class="carousel slide" id="LastUnitProd">
                            <div class="carousel-inner">
                            <?php
                            $unitsChunk = array_chunk($prodList, 3);
                            $i = 0;
                            foreach ($unitsChunk as $arr) {
                                ?>
                                <div class="item<?php if (!$i) echo " active"; ?>">
                                    <div class="thumbnails">
                                        <?php
                                        foreach ($arr as $unit):
                                            ?>
                                            <div class="col-sm-4 col-md-4">
                                                <div class="thumbnail unit">
                                                    <h4 class="unitListName<?php if(isset($unit['class'])){ echo ' '.$unit['class'];}?>"><a
                                                            href='<?php echo "/catalog/" . $unit['url']; ?>'><?php echo Room::subTitle($unit['pTitle'], 60); ?></a>
                                                    </h4>

                                                    <div class="unitListImg"><a
                                                            href='<?php echo "/catalog/" . $unit['url']; ?>'><img alt=""
                                                                                                                  class="unitImg"
                                                                                                                  src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a>
                                                    </div>
                                                    <div class="caption unitListBlock">
                                                        <div
                                                            class="unitListDesc"><?php echo strip_tags(htmlspecialchars_decode(Room::subDesc($unit['desc'], 150))); ?></div>
                                                        <div class="unitButtons"><a
                                                                href='<?php echo "/catalog/" . $unit['url']; ?>'
                                                                role="button"
                                                                class="btn btn-primary">Узнать больше</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        endforeach;
                                        $i++;
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="control-box"><a data-slide="prev" href="#LastUnitProd"
                                                    class="carousel-control left">
                                <img
                                    src="../../public/images/Arrow-Left-icon-white.png"/></a> <a data-slide="next"
                                                                                                 href="#LastUnitProd"
                                                                                                 class="carousel-control right">
                                <img
                                    src="../../public/images/Arrow-Right-icon-white.png"/></a></div>
                    </div>
                <?php else: ?>
                <div class="row">
                    <?php foreach ($prodList as $unit): ?>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail unit">
                                <div class="unitListImg"><a href='<?php echo "/" . $unit['url']; ?>'><img alt=""
                                                                                                          class="unitImg"
                                                                                                          src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a>
                                </div>
                                <div class="caption unitListBlock">
                                    <h4 class="unitListName"><a
                                            href='<?php echo "/" . $unit['url']; ?>'><?php echo $unit['pTitle']; ?></a>
                                    </h4>

                                    <div
                                        class="unitListDesc"><?php echo htmlspecialchars_decode(Room::subDesc($unit['desc'])); ?></div>
                                    <div class="unitButtons"><a href='<?php echo "/news/" . $unit['url']; ?>'
                                                                onclick="yaCounter37307950.reachGoal('NEWS');"
                                                                role="button"
                                                                class="btn btn-primary">Узнать больше</a></div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="faceNews">
    <div class="container">

        <h3 class="faceNewsH h1">НОВОСТИ</h3>
        <!-- Блок Последних новостей -->
        <?php
        if (isset($newsList) && count($newsList)): ?>
        <?php if (count($newsList) > 3): ?>
            <div class="carousel slide" id="LastUnitNews">
                <div class="carousel-inner">
                    <?php
                    $unitsChunk = array_chunk($newsList, 3);
                    $i = 0;
                    foreach ($unitsChunk as $arr) {
                        ?>
                        <div class="item<?php if (!$i) echo " active"; ?>">
                            <div class="thumbnails">
                                <?php
                                foreach ($arr as $unit):
                                    ?>
                                    <div class="col-sm-4 col-md-4">
                                        <div class="thumbnail unit">
                                            <div class="unitListImg">
                                                <a href='<?php echo "/news/" . $unit['url']; ?>'>
                                                    <img alt="" class="unitImg"
                                                         src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>">
                                                </a>
                                            </div>

                                            <h4 class="unitListName">
                                                <div class="date"><?php echo date('d/m/Y', $unit['date']); ?></div>
                                                <a href='<?php echo "/news/" . $unit['url']; ?>'>
                                                    <?php echo Room::subTitle($unit['pTitle'], 25); ?>
                                                </a>
                                            </h4>

                                            <div class="caption unitListBlock">
                                                <div class="unitListDesc">
                                                    <?php echo htmlspecialchars_decode(Room::subDesc($unit['desc'], 150)); ?>
                                                </div>

                                                <div class="unitButtons">
                                                    <a href='<?php echo "/news/" . $unit['url']; ?>'
                                                       role="button"
                                                       class="btn btn-danger">Подробнее</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach;
                                $i++; ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="control-box">
                    <a data-slide="prev" href="#LastUnitNews" class="carousel-control left">
                        <img src="/public/images/Arrow-Left-icon.png"/></a>
                    <a data-slide="next" href="#LastUnitNews" class="carousel-control right">
                        <img src="/public/images/Arrow-Right-icon.png"/></a>
                </div>
            </div>
        <?php else: ?>
        <div class="row">
            <?php foreach ($newsList as $unit): ?>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail unit">
                        <div class="unitListImg"><a href='<?php echo "/" . $unit['url']; ?>'><img alt=""
                                                                                                  class="unitImg"
                                                                                                  src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a>
                        </div>
                        <div class="caption unitListBlock">
                            <h4 class="unitListName"><a
                                    href='<?php echo "/" . $unit['url']; ?>'><?php echo $unit['pTitle']; ?></a>
                            </h4>

                            <div
                                class="unitListDesc"><?php echo htmlspecialchars_decode(Room::subDesc($unit['desc'])); ?></div>
                            <div class="unitButtons"><a href='<?php echo "/news/" . $unit['url']; ?>'
                                                        onclick="yaCounter37307950.reachGoal('NEWS');"
                                                        role="button"
                                                        class="btn btn-primary">Узнать больше</a></div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <?php endif; ?>
            <?php endif; ?>
        </div>

    </div>
</div>
<div class="faceBlock3">
    <div class="container">
        <hr>
        <div class="textContent">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
                    <div id="map" style="width: auto; height: 446px"></div>
                    <script type="text/javascript">
                        var map = new ymaps.Map("map", {
                            center: [55.76, 37.64],
                            zoom: 7
                        });
                    </script>
                    <script type="text/javascript">
                        ymaps.ready(init);
                        var myMap;

                        function init() {
                            myMap = new ymaps.Map("map", {
                                center: [51.664506, 36.102681],
                                zoom: 15
                            });

                            var myPlacemark = new ymaps.Placemark([51.664506, 36.102681], {
                                balloonContent: 'ООО «Курский аккумуляторный завод»'
                            }, {
                                iconImageHref: '/public/images/face/mapico.png',
                                iconImageSize: [50, 40]
                            });
                            myMap.geoObjects.add(myPlacemark);

                        }
                    </script>

                </div>
                <hr class="hidden-lg pad15">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 center">
                    <object type="application/x-shockwave-flash" data="/public/images/winter.swf" id="flash"
                            style="visibility: visible;" width="240" height="458">
                        <param name="wmode" value="transparent">
                        <param name="menu" value="false">
                    </object>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-8 col-lg-4 faceInfoBlocks">
                    <!-- blocks -->
                    <?php if (isset($newsList) && count($newsList)): ?>

                        <?php if (count($newsList) > 4): ?>
                            <div class="carousel slide" id="LastUnitNews">
                                <div class="carousel-inner">
                                    <?php
                                    $unitsChunk = array_chunk($newsList, 4);
                                    $i = 0;
                                    foreach ($unitsChunk as $arr) {
                                        ?>
                                        <div class="item<?php if (!$i) echo " active"; ?>">
                                            <div class="thumbnails">
                                                <?php
                                                foreach ($arr as $unit):
                                                    ?>
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="thumbnail unit">
                                                            <!--                                                            <div class="unitListImg"><a-->
                                                            <!--                                                                    href='-->
                                                            <?php //echo "/news/" . $unit['url'];
                                                            ?><!--'><img-->
                                                            <!--                                                                        alt="" class="unitImg"-->
                                                            <!--                                                                        src="/public/images/unit/-->
                                                            <?php //echo Room::showImg($unit['image']);
                                                            ?><!--"></a>-->
                                                            <!--                                                            </div>-->
                                                            <div class="caption unitListBlock">
                                                                <h4 class="unitListName faceNewsName"><a
                                                                        href='<?php echo "/news/" . $unit['url']; ?>'><?php echo Room::subTitle($unit['pTitle'], 40); ?></a>
                                                                </h4>

                                                                <div
                                                                    class="unitListDesc"><?php echo htmlspecialchars_decode(Room::subDesc($unit['desc'])); ?></div>
                                                                <div class="unitButtons"><a
                                                                        href='<?php echo "/news/" . $unit['url']; ?>'
                                                                        role="button"
                                                                        class="btn btn-primary">Читать</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                endforeach;
                                                $i++;
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="control-box"><a data-slide="prev" href="#LastUnitNews"
                                                            class="carousel-control left">
                                        <img
                                            src="../../public/images/Arrow-Left-icon.png"/></a> <a data-slide="next"
                                                                                                   href="#LastUnitNews"
                                                                                                   class="carousel-control right">
                                        <img
                                            src="../../public/images/Arrow-Right-icon.png"/></a></div>
                            </div>
                        <?php else: ?>
                            <div class="row">
                                <?php foreach ($newsList as $unit): ?>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail unit">
                                            <!--                                            <div class="unitListImg"><a href='-->
                                            <?php //echo "/news/" . $unit['url']; ?><!--'><img-->
                                            <!--                                                        alt="" class="unitImg"-->
                                            <!--                                                        src="/public/images/unit/-->
                                            <?php //echo Room::showImg($unit['image']); ?><!--"></a>-->
                                            <!--                                            </div>-->
                                            <div class="caption unitListBlock">
                                                <h4 class="unitListName faceNewsName"><a
                                                        href='<?php echo "/news/" . $unit['url']; ?>'><?php echo Room::subTitle($unit['pTitle'], 40); ?></a>
                                                </h4>

                                                <div
                                                    class="unitListDesc"><?php echo htmlspecialchars_decode(Room::subDesc($unit['desc'])); ?></div>
                                                <div class="unitButtons"><a
                                                        href='<?php echo "/news/" . $unit['url']; ?>'
                                                        role="button" class="btn btn-primary">Читать</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <hr>

                </div>

            </div>
        </div>
    </div>
</div>
<script>
    function activeTab(num) {
        $RoomWrap('#myTab li:eq(' + num + ') a').tab('show');
    }
</script> 