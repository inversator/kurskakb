<?php if (isset($questions) && count($questions)):
    $i = 0;
    ?>
    <div class="panel-group" id="accordion">
        <?php foreach ($questions as $ask):
            $i++; ?>
            <div class="panel panel-default">
                <div class="panel-heading qst">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                           href="#collapse-<?php echo $i;?>">
                            <span class="date pull-right label label-info"><?php echo date('d m Y', $ask['date']); ?></span>
                            <span class="name"><?php echo $ask['user']; ?></span>:
                            <span class="title"><?php echo $ask['question']; ?></span>

                        </a>
                    </h4>
                </div>
                <div id="collapse-<?php echo $i;?>" class="panel-collapse collapse">
                    <div class="panel-body ans">
                        <div class="ans">
                            <div class="name text-info h4"><?php echo $ask['manager']; ?> <span class="func label label-primary h5"><?php echo $ask['m_func']; ?></span></div>

                            <div class="title"><?php echo $ask['answer']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>