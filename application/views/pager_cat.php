<?php
if (count($units) > 0) {
    foreach ($units as $unit):
        ?>
        <div class="col-sm-6 col-md-4">
        <div class="thumbnail unit">
        <h4 class="unitListName"><a
                href='<?php echo "/" . $url . "/" . $unit['url']; ?>'><?php echo $unit['pTitle']; ?></a>
        </h4>
        <div class="unitListImg">
        <a href='<?php echo "/" . $url . "/" . $unit['url']; ?>'><img alt=""
                                                                      class="unitImg"
                                                                      src="/public/images/unit/<?php
                                                                      if ($unit['image']) {
                                                                          echo 'thumbs/' . Room::showImg($unit['image']);
                                                                      } else {
                                                                          echo 'no-img.png';
                                                                      }
                                                                      ?>"></a>
        </div>
        <div class="caption unitListBlock">


            <div
                class="unitListDesc"><?php echo strip_tags(htmlspecialchars_decode(Room::subDesc($unit['desc'], 100))); ?></div>
            <div class="unitButtons">

            </div>
        </div>
        </div>
        </div>
    <?php endforeach;
} else {
    ?>
    <p class='alert alert-warning'>Материалы не найдены</p>
<?php } ?>
