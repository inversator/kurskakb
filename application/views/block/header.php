<div class="container">
    <div class="row header">


        <div class="col-xs-12 col-sm-4 text-center">
            <a class="logo" href="/" title="Рязцветмет">
                <img class="imageRubber" src="/public/images/logo.png" title="Рязцветмет" alt="Рязцветмет"/>
            </a>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div id="topInfoText">
                <span class="glyphicon glyphicon-send"></span>&nbsp; <a href="mailto:info@accumkursk.ru">info@accumkursk.ru</a><br>
                <span class="glyphicon glyphicon-map-marker"></span>&nbsp; 305026, г. Курск, пр. Ленинского Комсомола,
                40<br>
                <span class="glyphicon glyphicon-time"></span>&nbsp; Режим работы: c 9 до 18
            </div>
        </div>

        <div class="col-xs-12 col-sm-4 headBlock3">
            <ul class="nav navbar-nav navbar-right">
                <li class="topPhone">
                    <span><span class="glyphicon glyphicon-earphone"></span>&nbsp; +7 (4712) 22-77-88</span>
                </li>
                <li class="callMe">
                    <button class="btn btn-warning" data-target="#callmeModal" data-toggle="modal">Перезвоните мне
                    </button>
                </li>
            </ul>
        </div>

    </div>
</div>