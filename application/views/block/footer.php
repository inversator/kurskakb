<div id="infoBlock">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 fBlock1">
                <div class="logo-bottom">
                    <a href="/company/kurskiy-zavod"><img class="imageRubber" src="/public/images/face/kursk.png" width="100" /></a>
                    <a href="/company/istochnik-toka-kurskiy"><img style="float: right" class="imageRubber" src="/public/images/face/istok.png" width="150" /></a>
                  </div>
                <hr>
                <p>Объединенная производственная площадка</p>
            </div>
            <div class="col-xs-12 col-sm-4 fBlock2">
                <h3>Навигация</h3>
                <hr>
                <div class="row">
                    <div class="col-xs-6">
                        <ul class="list-unstyled">
                            <li><a href="/sertifikaty-i-licenzii"><span>Сертификаты и лицензии</span></li>
                            <li><a href="/diplomy-i-nagrady"><span>Дипломы и награды</span></a></li>
                            <li><a href="/quality"><span>Положение по качеству для поставщиков</span></a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <ul class="list-unstyled">
                            <li><a href="/article/kakuyu-vybrat-akb-i-kak-pravil-no-eto-sdelat"><span>Как подобрать аккумулятор</span></a></li>
                            <li><a href="/article/usloviya-ekspluatacii-starternyh-akb"><span>Условия эксплуатации АКБ</span></a></li>
                            <li><a href="/article/mnimye-neispravnosti-akb"><span>«Мнимые» неисправности АКБ</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 fBlock3">
                <h3>Контактная информация</h3>
                <hr>
                <ul class="list-unstyled list">
                    <li>
                        <span class="glyphicon glyphicon-map-marker"></span>&nbsp;305026, г. Курск, пр. Ленинского Комсомола, 40
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-earphone"></span>&nbsp; +7 (4712) 22-77-88
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-send"></span>&nbsp;<a href="mailto:info@accumkursk.ru">info@accumkursk.ru</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</div>
<footer id="footer">
    <div class="container">
        <div class="row">
            <span class="copypast col-sm-12 col-xs-12 text-center">
info@accumkursk.ru<br>
&copy; Курский завод «Аккумулятор», 2010 </span>
        </div>
    </div>
</footer>
