<div class="row">
    <div id="scroll-menu" class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#midMenu">
                    <span class="sr-only">Меню</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="midMenu">
                <!--
            <ul class="nav navbar-nav nav-pills">
                <?php echo $lis; ?>
            </ul>
-->
                <ul class="nav navbar-nav">
                    <li><a href="/"><span>ГЛАВНАЯ</span></a></li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"
                           aria-expanded="false"><span>КОМПАНИЯ</span><b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/company/kurskiy-zavod"><span>Курский завод «Аккумулятор»</span></a></li>
                            <li><a href="/company/istochnik-toka-kurskiy"><span>ИСточник ТОка Курский</span></a></li>
                            <li><a href="/sertifikaty-i-licenzii"><span>Сертификаты и лицензии</span></a></li>
                            <li><a href="/diplomy-i-nagrady"><span>Дипломы и награды</span></a></li>
                            <li><a href="/quality"><span>Положение по качеству для поставщиков</span></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="/catalog" aria-expanded="false"><span>ПРОДУКЦИЯ</span><b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/catalog/auto"><span>Стартерные АКБ для авто-мототехники</span></a></li>
                            <li><a href="/catalog/industrial"><span>Промышленные АКБ</span></a></li>
                        </ul>
                    </li>
                    <li><a href="/news"><span>НОВОСТИ</a></li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="/cooperation"
                           aria-expanded="false"><span>ВСЕ ОБ АККУМУЛЯТОРАХ</span><b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/article/kakuyu-vybrat-akb-i-kak-pravil-no-eto-sdelat"><span>Как подобрать аккумулятор</span></a></li>
                            <li><a href="/article/usloviya-ekspluatacii-starternyh-akb"><span>Условия эксплуатации стартерных АКБ</span></a></li>
                            <li><a href="/article/mnimye-neispravnosti-akb"><span>«Мнимые» неисправности АКБ</span></a></li>
                        </ul>
                    </li>
                    <li><a href="/dealers"><span>ДИЛЕРСКАЯ СЕТЬ</span></a></li>
                    <li><a href="/faq"><span>ВОПРОС ОТВЕТ</span></a></li>
                    <li><a href="/contacts"><span>КОНТАКТЫ</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>