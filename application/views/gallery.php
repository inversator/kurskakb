<div class="bg-info"><div class="container"><div class="row"></div><a href="">Главная</a> > <a href="/company/kurskiy-zavod">Компания</a> > <?php echo $self['pTitle']; ?></div></div>

<div class="container" id="gallery">
    <h1><?php echo $self['pTitle']; ?></h1>

    <div class="row">
        <?php
        foreach ($images as $image): ?>
            <div class="col-xs-12 col-sm-4">
                <div class="block">
                    <div class="licensesImage"><a href="/public/images/gallery/<?php echo $image['name']; ?>"
                                                  class="lightview" data-lightview-group="history"
                                                  data-lightview-title=""
                                                  data-lightview-caption="<?php echo htmlspecialchars($image['desc']); ?>">
                            <img src="/public/images/gallery/thumbs/<?php echo $image['name']; ?>" alt="">
                            <span class="zoomImage"> <span class="glyphicon glyphicon-zoom-in"></span> </span> </a>
                    </div>
                    <div class="desc"><?php echo $image['desc']; ?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="desc">
                <?php echo $self['desc']; ?>
            </div>
        </div>
    </div>
</div>