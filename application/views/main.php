<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
    </script>

    <!-- Подключаем бутстрап -->
    <link href="<?php echo URL::base(); ?>bootstrap/css/bootstrap.css"
          rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<?php echo URL::base(); ?>bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo URL::base(); ?>public/js/main.js"></script>

    <script src="<?php echo URL::base(); ?>public/admin/js/jquery.datetimepicker.full.js"
            type="text/javascript"></script>

    <link href="<?php echo URL::base(); ?>public/admin/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>

    <!-- Подключаем light box -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/public/js/excanvas/excanvas.js"></script>
    <![endif]-->
    <script type="text/javascript" src="/public/js/spinners/spinners.min.js"></script>
    <script type="text/javascript" src="/public/js/lightview/lightview.js"></script>

    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>


    <link rel="stylesheet" type="text/css" href="/public/css/lightview/lightview.css"/>

    <?php foreach ($styles as $style): ?>
        <link href="/public/css/<?php echo $style; ?>.css"
              rel="stylesheet" type="text/css"/>
    <?php endforeach; ?>

    <title><?php echo !empty($bTitle) ? $bTitle : 'Рязцветмет'; ?></title>

</head>

<body>
<?php echo $header; ?>

<?php echo $navigation; ?>

    <?php echo $content; ?>


<?php echo $footer; ?>


</body>

<!-- Это модалка вопроса-->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="formQuestRes"></div>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Задать вопрос</h4>
            </div>
            <div class="modal-body">
                <form id="formQuestion">
                    <p>Здесь Вы можете задать интересующий вопрос. Наши менеджеры свяжутся с Вами ближайшее время.</p>

                    <input type="hidden" name="type" value="Вопрос">
                    <input type="hidden" name="subject" value="question">

                    <div class="form-group" own="name">
                        <error></error>
                        <input onchange="checkField('formQuestion')" class="form-control" type="text1" name="name"
                               value="" placeholder="Ваше имя">
                    </div>

                    <div class="form-group" own="phone">
                        <error></error>
                        <input onchange="checkField('formQuestion')" class="form-control" type="text1" name="phone"
                               value="" placeholder="Ваш телефон">
                    </div>

                    <div class="form-group" own="email">
                        <error></error>
                        <input onchange="checkField('formQuestion')" class="form-control" type="text1" name="email"
                               value="" placeholder="Ваш e-mail">
                    </div>

                    <div class="form-group" own="comment">
                        <error></error>
                        <strong>Ваш вопрос:</strong>
                        <textarea class="form-control" rows="10" name="comment"
                                  onchange="checkField('formQuestion')"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button"
                        onclick="AjaxResForm('formQuestRes', 'formQuestion', '/sender/send'); return false;"
                        class="btn btn-primary">Спросить
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Это модалка телефона-->
<div class="modal fade" id="callmeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="formCallRes"></div>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Мы перезвоним Вам!</h4>
            </div>
            <div class="modal-body">
                <form id="formCallme">
                    <p>Оставьте свой номер телефона и мы свяжемся с Вами в ближайшее время!</p>

                    <input type="hidden" name="type" value="Заказ звонка">
                    <input type="hidden" name="subject" value="callMe">

                    <div class="form-group" own="phone">
                        <error></error>
                        <input onchange="checkField('formCallme')" class="form-control" type="text1" name="phone"
                               value="" placeholder="Ваше телефон">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" onclick="AjaxResForm('formCallRes', 'formCallme', '/sender/send'); return false;"
                        class="btn btn-warning">Перезвонить мне
                </button>
            </div>
        </div>
    </div>
</div>
</html>