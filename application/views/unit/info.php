<link rel="stylesheet" href="../public/jqzoom/css/jquery.jqzoom.css" type="text/css">
<script src="../public/jqzoom/js/jquery-1.6.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/public/css/thumbelina.css"/>
<script type="text/javascript" src="/public/js/thumbelina.js"></script>
<script type="text/javascript">
    $(function () {
        $('#slider1').Thumbelina({
            $bwdBut: $('#slider1 .left'), // Selector to left button.
            $fwdBut: $('#slider1 .right')    // Selector to right button.
        });
    })
</script>
<div class="bg-info"><div class="container"><div class="row"></div><a href="">Главная</a> > <a href="/<?php echo $category; ?>"><?php echo $cat[$category]['pTitle'];?></a> > <?php echo $title; ?></div></div>
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button class="btn btn-primary btn-xs" data-toggle="offcanvas" type="button">Инфо</button>
            </p>
            <div class="page-header">

                <?php if (in_array($category, array('news'))): ?>
                    <span class="label label-info newsDate">
                        <?php echo date('d/m/Y', $date); ?>
                    </span>
                <?php endif; ?>
                <h1><?php echo $title; ?></h1>

                <div class="row">
                    <div class="unitImgBlock col-sm-6">
                        <div class="clearfix">
                            <?php
                            if (isset($mainImage)):
                            if ($mainImage['width'] > 400 || $mainImage['height'] > 400): ?>
                                <a href="/public/images/unit/<?php echo $mainImage['name']; ?>"
                                   class="jqzoom"
                                   rel='gal1'
                                   title="<?php echo $title; ?>">
                                    <img src="/public/images/unit/<?php echo $mainImage['name']; ?>"
                                         class="img-thumbnail mainImgUnit">
                                </a>
                            <?php else: ?>
                                <div class="img-thumbnail pull-left">
                                    <img src="/public/images/unit/<?php echo $mainImage['name']; ?>"
                                         width="<?php echo $mainImage['width']; ?>"/>
                                </div>
                            <?php endif; ?>
                            <?php else: ?>
                                <div class="img-thumbnail pull-left">
                                    <img src="/public/images/unit/no-img.png" />
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php if (count($images) > 1): ?>
                            <br/>
                            <div class="clearfix">
                                <div id="slider1">
                                    <div class="thumbelina-but horiz left">&#706;</div>
                                    <ul id="thumblist" class="clearfix">
                                        <?php foreach ($images as $image): ?>
                                            <li class="img-thumbnail">
                                                <a href='javascript:void(0);'
                                                   rel="{
                                                     gallery: 'gal1',
                                                     smallimage: '../public/images/unit/<?php echo $image['name']; ?>',
                                                     largeimage: '../public/images/unit/<?php echo $image['name']; ?>'
                                                   }">
                                                    <img class="img-rounded" width='120'
                                                         src='../public/images/unit/<?php echo $image['name']; ?>'>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <div class="thumbelina-but horiz right">&#707;</div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-6">
                        <p class="lead"><?php echo htmlspecialchars_decode($desc); ?></p>

                        <div class="properties">
                            <table class="table table-striped">
                                <?php foreach ($properties as $property): ?>
                                    <tr>
                                        <td><?php echo $property['alias']; ?></td>
                                        <td><?php echo $property['value']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fullText"><?php echo htmlspecialchars_decode($text); ?></div>
            <div class="col-xs-12 col-sm-6"></div>
            <?php if (isset($docs) && count($docs)): ?>
                <div class="col-xs-12">
                    <table class="table table-striped">
                        <tr>
                            <th>Файл</th>
                            <th>Тип</th>
                            <th>Размер</th>
                            <th>Открыть/Скачать</th>
                        </tr>
                        <?php foreach ($docs as $doc): ?>
                            <tr>
                                <td><?php echo $doc->description; ?></td>
                                <td><?php echo $doc->type; ?></td>
                                <td><?php echo Room::dataSize($doc->size); ?></td>
                                <td align="center"><a href="/public/files/<?php echo $doc->file; ?>"
                                                      target="_blank"><span
                                            class="glyphicon glyphicon-download-alt"></span></a></td>
                            </tr>
                        <?php endforeach;
                        ?>
                    </table>
                </div>
            <?php endif; ?>
            <?php if (isset($units) && count($units)): ?>
                <hr>
                <h3>Похожие материалы:</h3>


                <?php if (count($units) > 3): ?>
                    <div class="carousel slide" id="similarProd">
                        <div class="carousel-inner">
                            <?php
                            $unitsChunk = array_chunk($units, 3);
                            $i = 0;
                            foreach ($unitsChunk as $arr) {
                                ?>
                                <div class="item<?php if (!$i) echo " active"; ?>">
                                    <div class="thumbnails">
                                        <?php
                                        foreach ($arr as $unit):
                                            ?>

                                            <div class="col-sm-4 col-md-4">
                                                <div class="thumbnail unit">
                                                    <h4 class="unitListName">
                                                        <a title="<?php echo $unit['pTitle']; ?>"
                                                           href='<?php echo "/" . $unit['url']; ?>'><?php echo Room::subTitle($unit['pTitle']); ?></a>
                                                    </h4>

                                                    <div class="caption unitListBlock">
                                                        <div class="unitListDesc">
                                                            <?php echo strip_tags(htmlspecialchars_decode(Room::subDesc($unit['desc'], 100))); ?>
                                                        </div>
                                                    </div>
                                                    <div class="unitListImg">
                                                        <a href='<?php echo "/" . $unit['url']; ?>'>
                                                            <img alt="" class="unitImg"
                                                                 src="/public/images/unit/thumbs/<?php echo Room::noImg($unit['image']); ?>">
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>

                                            <?php
                                        endforeach;
                                        $i++;
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                        </div>
                        <div class="control-box">
                            <a data-slide="prev" href="#similarProd" class="carousel-control left"> <img
                                    src="../../public/images/Arrow-Left-icon.png"/></a>
                            <a data-slide="next" href="#similarProd" class="carousel-control right"> <img
                                    src="../../public/images/Arrow-Right-icon.png"/></a>
                        </div>
                    </div>
                <?php else: ?>

                    <div class="row">
                        <div class="carousel slide" id="similarProd">
                            <?php foreach ($units as $unit): ?>
                                <div class="col-sm-4 col-md-4">
                                    <div class="thumbnail unit">
                                        <h4 class="unitListName">
                                            <a title="<?php echo $unit['pTitle']; ?>"
                                               href='<?php echo "/" . $unit['url']; ?>'><?php echo Room::subTitle($unit['pTitle']); ?></a>
                                        </h4>

                                        <div class="caption unitListBlock">
                                            <div class="unitListDesc">
                                                <?php echo strip_tags(htmlspecialchars_decode(Room::subDesc($unit['desc'], 100))); ?>
                                            </div>
                                        </div>
                                        <div class="unitListImg">
                                            <a href='<?php echo "/" . $unit['url']; ?>'>
                                                <img alt="" class="unitImg"
                                                     src="/public/images/unit/thumbs/<?php echo Room::noImg($unit['image']); ?>">
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>

            <?php endif; ?>

        </div>
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar"
             role="navigation"> <?php echo $rigthMenu; ?> </div>
    </div>

</div>
<script src="../public/jqzoom/js/jquery.jqzoom-core.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.jqzoom').jqzoom({
            zoomType: 'standard',
            title: false,
            lens: true,
            preloadImages: false,
            alwaysOn: false,
            zoomWidth: 408,
            yOffset: 300
        });
    });

    function reConfigZoom(width, height) {
        $(document).ready(function () {
            $('.jqzoom').jqzoom({
                zoomType: 'standard',
                title: false,
                lens: true,
                preloadImages: false,
                alwaysOn: false,
                zoomWidth: 408,
                yOffset: 300
            });
        });
    }

</script> 
