<div class="row">
    <div class="col-xs-6">   
        <form role="form" class="form-reg" id="regForm">
            <h2 class="form-signin-heading">Регистрация</h2>
            <div id="result_id"></div>
            <input type="text" value="" name="username" placeholder="Логин" class="form-control" />

            <input type="text" value="" name="email" placeholder="Email" class="form-control" />

            <input type="password" value="" name="password" placeholder="Придумайте пароль" class="form-control" />

            <input type="password" value="" name="password_confirm" placeholder="Подтвердите пароль" class="form-control" />

            <button name="registration" onclick="AjaxRegForm('result_id', 'regForm', '/users/registration');
                return false;" class="btn btn-lg btn-primary btn-block">Зарегистрироваться</button>
        </form>
    </div>
    <div class="col-xs-6 note">
        <h3 id="grid-intro">Правила регистрации</h3>
        <p>Для регистрации на сайте заполните следующие поля:</p>
        <ul>
            <li>Логин — имя Вашей учетной записи. Может быть как подлинным именем или фамилией, так и вымышленным псевдонимом. <span class="text-danger">Разрешается использовать только латинские символы и цифры.</span></li>
            <li>Email — адрес вашей текущей электронной почты. На неё будет отправлена ссылка для подтверждения регистрации, а так же, в случае Вашего согласия, новостные расссылки.</li>
            <li>Пароль — введите набор знаков для подтверждения личности. <span class="text-danger">Не менее 8-ми символов!</span></li>
            <li>Повторите пароль — введите пароль повторно. Так мы удостоверимся что вы вводили его верно.</li>
            
        </ul>
    </div>
</div>