
<div id="vacancy">
    <div class="container">
        <h1>Дилерская сеть</h1>

        <div class="fullText">
            <p class="bg-info">
                Обращаем ваше внимание, что у нашей компании на территории РФ есть два генеральных дистрибьютора
                федерального уровня, реализующих стартерные аккумуляторные батареи. Напрямую по данному направлению в РФ
                мы работаем только с автозаводами и организациями, входящими в структуру МО РФ.
            </p>

            <h5>Генеральными дистрибьюторами предприятия по стартерным АКБ на территории Российской Федерации
                являются:</h5>

            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="thumbnail pad15">
                        <h5>по торговой марке "ISTOK"</h5>

                        <span class="h2 text-primary">ООО "Эксперт"</span><br>

                        <span class="text-info h3">Региональный отдел продаж:</span><br>

                        <span class="h4">тел./факс: +7 (863) 237-03-30</span><br>

                        <span><a href="mailto:rostov@tdwesta.ru">rostov@tdwesta.ru</a></span><br>

                        <span class="text-info h3">Московский отдел продаж:</span><br>

                        <span class="h4">тел.: +7 (495) 646-74-77</span><br>

                        <span class="h4">факс: +7 (495) 306-87-79</span><br>

                        <span><a href="mailto:moscow@tdwesta.ru">moscow@tdwesta.ru</a></span><br>

                        <span>Россия, 344090, г. Ростов-на-Дону, ул. Доватора, 148, офис 417</span><br>

                        <span><a href="www.tdwesta.ru" target="_blank">www.tdwesta.ru</a></span>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12">
                    <div class="thumbnail pad15">
                        <h5>по торговой марке "Курский Аккумулятор"</h5>

                        <span class="h2 text-primary"> ООО «БатБаза»</span><br>

                        <span class="text-info h3">Региональный отдел продаж:</span><br>

                        <span class="h4"> тел./факс: +7 (495) 737-05-48</span><br>

                        <span><a href="mailto:inbox@batbaza.ru">inbox@batbaza.ru</a></span><br>

                        <span class="h3 text-info">Московский отдел продаж:</span><br>

                        <span class="h4">тел./факс: +7 (495) 737-69-40</span><br>

                        <span><a href="mailto:koordinator@batbaza.ru"
                                 target="_blank">koordinator@batbaza.ru</a></span><br>

                        <span>Россия, 105484, г. Москва, 16-я Парковая, 21 корп. 1</span><br>

                        <span><a href="www.batbaza.ru">www.batbaza.ru</a></span>
                    </div>
                </div>
            </div>
            <h2 class="text-warning">Внимание</h2>

            <p class="text-warning">Для поиска дилеров по стартерным АКБ (ближнее зарубежье) и по промышленным АКБ
                (Российская Федерация и
                ближнее зарубежье) просим воспользоваться формой, расположенной ниже, помня, что головные
                представительства крупных отечественных компаний, как правило, всегда есть в г. Москве (касается
                промышленных АКБ)</p>
        </div>
        <div class="row">
            <div class="col-xs-12">

            </div>
        </div>
    </div>
</div>
</div>
<div id="map"></div>
<div><p class="bg-primary h5 pad15 text-center nomargin">В случае отсутсвия дилера на интересующей Вас территории, обращайтесь в
    отдел продаж нашего предприятия по следующим телефонам: группа стартерных АКБ - +7 (4712) 22-77-90, группа
    промышленных АКБ - +7 (4712) 22-77-89 добавочный "2".</p>
</div>

<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
    ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map('map', {
                center: [50.76, 50.64],
                zoom: 4
            }, {
                searchControlProvider: 'yandex#search'
            }),
            objectManager = new ymaps.ObjectManager({
                // Чтобы метки начали кластеризоваться, выставляем опцию.
                clusterize: true,
                // ObjectManager принимает те же опции, что и кластеризатор.
                gridSize: 32,
            });

        // Чтобы задать опции одиночным объектам и кластерам,
        // обратимся к дочерним коллекциям ObjectManager.
        objectManager.objects.options.set('preset', 'islands#greenDotIcon');
        objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
        myMap.geoObjects.add(objectManager);

        $.ajax({
            url: "/dealer/get"
        }).done(function (data) {
            objectManager.add(data);
        });
    }
</script>