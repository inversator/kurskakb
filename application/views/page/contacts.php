
<div id="contacts" class="container">
    <h1>Контакты</h1>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#kaz" data-toggle="tab"><h2>ООО «Курский аккумуляторный завод»</h2></a></li>
        <li><a href="#istok" data-toggle="tab"><h2>ООО «ИСТОК+»</h2></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="kaz">
            <table class="table">
                <tr>
                    <td>Адрес</td>
                    <td>Россия, 305026, г. Курск, пр-т Ленинского Комсомола, 40, офис 116</td>
                </tr>
                <tr>
                    <td>Телефон</td>
                    <td>+7 (4712) 22-77-88</td>
                </tr>
                <tr>
                    <td>Е-mail</td>
                    <td><a href="mailto:info@accumkursk.ru">info@accumkursk.ru</a></td>
                </tr>
            </table>
            <table class="table table-striped table-bordered">

                <tr class="info">
                    <th>Правила набора номера телефона:</th>
                    <th>Набираем код и 6-тизначный номер. Попадаем в голосовое меню. Жмем «1» и набираем добавочный номер.</th>
                    <th>Если Вы не знаете добавочный, то выберите пожалуйста 1 из озвученных в меню вариантов</th>
                </tr>

                <tr>
                    <td>Режим работы заводоуправления:</td>
                    <td>9.00 – 18.00 перерыв с 13.00 до 14.00</td>
                    <td>технические службы 8.00 – 16.45 перерыв с 12.30 до 13.15</td>
                </tr>

                <tr>
                    <td>Генеральный директор</td>
                    <td>Рыбянцев Андрей Александрович</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «5603»</td>
                </tr>

                <tr>
                    <td>Главный инженер</td>
                    <td>Степанов Александр Васильевич</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «5601»</td>
                </tr>

                <tr>
                    <td>Главный технолог-начальник технического отдела</td>
                    <td>Гречушников Евгений Александрович</td>
                    <td>тел. +7 (4712) 22-77-93</td>
                </tr>

                <tr>
                    <td>Директор департамента по закупкам</td>
                    <td>Науменко Константин Юрьевич</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «5601»</td>
                </tr>

                <tr>
                    <td>Начальник отдела продаж промышленных АКБ и первичного рынка</td>
                    <td>Колесников Валерий Николаевич</td>
                    <td>тел. +7 (4712) 22-77-89 добавочный «0» факс +7 (4712) 22-77-87</td>
                </tr>

                <tr>
                    <td>Начальник службы качества</td>
                    <td>Шумский Александр Георгиевич</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «5501»</td>
                </tr>

                <tr>
                    <td>Отдел продаж и развития рынка стартерных АКБ</td>
                    <td>Умеренкова Валентина Алексеевна</td>
                    <td>тел. +7 (4712) 22-77-90</td>
                </tr>

                <tr>
                    <td>Отдел продаж (группа промышленных АКБ)</td>
                    <td>Сергеева Людмила Алексеевна</td>
                    <td>тел. +7 (4712) 22-77-89 доб. «2» или 22-77-88 доб. «3603» E-mail: sergeeva@accumkursk.ru</td>
                </tr>

                <tr>
                    <td>Начальник отдела материально-технического снабжения</td>
                    <td>Алпатов Максим Александрович</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «5001»</td>
                </tr>

                <tr>
                    <td>Начальник отдела управления персоналом (отдел кадров)</td>
                    <td>Медведева Галина Васильевна</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «9501»</td>
                </tr>

                <tr>
                    <td>Заводской магазин «ИСТОЧНИК» (стартерные АКБ в розницу)</td>
                    <td>Людмила Николаевна, Елена Михайловна</td>
                    <td>тел. +7 (4712) 22-77-89 добавочный «1», пн-пт с 9.00 до 19.00, сб-вс с 9.00 до 17.00</td>
                </tr>

                <tr>
                    <td>Закупка лома аккумуляторов (юр. лица)</td>
                    <td>Попов Сергей Васильевич</td>
                    <td>тел. +7(4712) 22-77-88 добавочный «7201», моб. +7-920-715-42-15</td>
                </tr>

                <tr>
                    <td>Пункт приема лома свинцовых АКБ (физ. лица)	</td>
                    <td>Нина Николаевна, Зоя Васильевна</td>
                    <td>тел. +7(4712) 22-77-88 добавочный «7204»</td>
                </tr>

            </table>
            <div id="map" style="width: auto; height: 446px"></div>
            <script type="text/javascript">
                var map = new ymaps.Map("map", {
                    center: [55.76, 37.64],
                    zoom: 7
                });
            </script>
            <script type="text/javascript">
                ymaps.ready(init);
                var myMap;

                function init() {
                    myMap = new ymaps.Map("map", {
                        center: [51.664506, 36.102681],
                        zoom: 15
                    });

                    var myPlacemark = new ymaps.Placemark([51.664506, 36.102681], {
                        balloonContent: 'ООО «Курский аккумуляторный завод»'
                    }, {
                        iconImageHref: '/public/images/face/mapico.png',
                        iconImageSize: [50, 40]
                    });
                    myMap.geoObjects.add(myPlacemark);

                }
            </script>
        </div>
        <div class="tab-pane" id="istok">
            <table class="table">
                <tr>
                    <td>Адрес</td>
                    <td>Россия, 305026, г. Курск, пр-т Ленинского Комсомола, 40, офис 305</td>
                </tr>
                <tr>
                    <td>Телефон</td>
                    <td>+7 (4712) 22-77-88</td>
                </tr>
                <tr>
                    <td>Е-mail</td>
                    <td><a href="mailto:info@accumkursk.ru">info@accumkursk.ru</a></td>
                </tr>
            </table>
            <table class="table table-striped table-bordered">

                <tr class="info">
                    <th>Правила набора номера телефона:</th>
                    <th>Набираем код и 6-тизначный номер. Попадаем в голосовое меню. Жмем «1» и набираем добавочный номер.</th>
                    <th>Если Вы не знаете добавочный, то выберите пожалуйста 1 из озвученных в меню вариантов</th>
                </tr>

                <tr>
                    <td>Режим работы заводоуправления:</td>
                    <td>9.00 – 18.00 перерыв с 13.00 до 14.00</td>
                    <td>технические службы 8.00 – 16.45 перерыв с 12.30 до 13.15</td>
                </tr>

                <tr>
                    <td>Генеральный директор</td>
                    <td>Косенко Лариса Евгеньевна</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «5601»</td>
                </tr>

                <tr>
                    <td>Директор департамента по закупкам</td>
                    <td>Науменко Константин Юрьевич</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «5601»</td>
                </tr>

                <tr>
                    <td>Главный технолог-начальник технического отдела</td>
                    <td>Гречушников Евгений Александрович</td>
                    <td>тел. +7 (4712) 22-77-93</td>
                </tr>

                <tr>
                    <td>Начальник отдела продаж промышленных АКБ и первичного рынка</td>
                    <td>Колесников Валерий Николаевич</td>
                    <td>тел. +7 (4712) 22-77-89 добавочный «0» факс +7 (4712) 22-77-87</td>
                </tr>

                <tr>
                    <td>Начальник службы качества</td>
                    <td>Шумский Александр Георгиевич</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «5501»</td>
                </tr>

                <tr>
                    <td>Начальник отдела продаж и развития рынка стартерных АКБ</td>
                    <td>Нестеренко Артем Владимирович</td>
                    <td>тел. +7 (4712) 22-77-90</td>
                </tr>

                <tr>
                    <td>Отдел продаж (группа промышленных АКБ)</td>
                    <td>Сергеева Людмила Алексеевна</td>
                    <td>тел. +7 (4712) 22-77-89 доб. «2» или 22-77-88 доб. «3603» E-mail: sergeeva@accumkursk.ru</td>
                </tr>

                <tr>
                    <td>Начальник отдела материально-технического снабжения</td>
                    <td>Алпатов Максим Александрович</td>
                    <td>+7 (4712) 22-77-88 добавочный «5001»</td>
                </tr>

                <tr>
                    <td>Начальник отдела управления персоналом (отдел кадров)</td>
                    <td>Медведева Галина Васильевна</td>
                    <td>тел. +7 (4712) 22-77-88 добавочный «9501»</td>
                </tr>

                <tr>
                    <td>Заводской магазин «ИСТОЧНИК» (стартерные АКБ в розницу)</td>
                    <td>Людмила Николаевна, Елена Михайловна</td>
                    <td>тел. +7 (4712) 22-77-89 добавочный «1», пн-пт с 9.00 до 19.00, сб-вс с 9.00 до 17.00</td>
                </tr>

                <tr>
                    <td>Закупка лома аккумуляторов (юр. лица)</td>
                    <td>Попов Сергей Васильевич</td>
                    <td>тел. +7(4712) 22-77-88 добавочный «7201», моб. +7-920-715-42-15</td>
                </tr>

                <tr>
                    <td>Пункт приема лома свинцовых АКБ (физ. лица)</td>
                    <td>Нина Николаевна, Зоя Васильевна</td>
                    <td>тел. +7(4712) 22-77-88 добавочный «7204»</td>
                </tr>

            </table>
            <div id="mapIstok" style="width: auto; height: 446px"></div>
            <script type="text/javascript">
                var map = new ymaps.Map("mapIstok", {
                    center: [55.76, 37.64],
                    zoom: 7
                });
            </script>
            <script type="text/javascript">
                ymaps.ready(init);
                var myMap;

                function init() {
                    myMap = new ymaps.Map("mapIstok", {
                        center: [51.664506, 36.102681],
                        zoom: 15
                    });

                    var myPlacemark = new ymaps.Placemark([51.664506, 36.102681], {
                        balloonContent: 'ООО «Курский аккумуляторный завод»'
                    }, {
                        iconImageHref: '/public/images/face/mapico.png',
                        iconImageSize: [50, 40]
                    });
                    myMap.geoObjects.add(myPlacemark);

                }
            </script>
        </div>
    </div>
<h3>Обратная связь</h3>
    <form id="formQuestion" class="row">
        <div class="col-xs-12 col-sm-6"><input name="type" value="Вопрос" type="hidden"> <input name="subject" value="question" type="hidden">
            <div class="form-group" own="name"><error></error> <input onchange="checkField('formQuestion')" class="form-control" name="name" value="" placeholder="Ваше имя" type="text1"></div>
            <div class="form-group" own="phone"><error></error> <input onchange="checkField('formQuestion')" class="form-control" name="phone" value="" placeholder="Ваше телефон" type="text1"></div>
            <div class="form-group" own="email"><error></error> <input onchange="checkField('formQuestion')" class="form-control" name="email" value="" placeholder="Ваше e-mail" type="text1"></div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group"><textarea class="form-control" name="comment" rows="4" placeholder=" Ваш вопрос"></textarea></div>
            <button type="button" onclick="AjaxResForm('formQuestRes', 'formQuestion', '/sender/send'); return false;" class="btn btn-primary">Спросить</button>
        </div>
    </form>
</div>
