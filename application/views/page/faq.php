<div id="faq" class="container">
    <div class="row">
        <h1>Вопрос: Ответ</h1>

        <div class="filter" type="page" slug="Question" cat="">
        </div>
        <div class="paginationBlock"><?php echo $pagination; ?></div>
        <hr>
        <div id="unitsBlock">
            <?php if (isset($questions) && count($questions)):
                $i = 0;
                ?>
                <div class="panel-group" id="accordion">
                    <?php foreach ($questions as $ask):
                        $i++; ?>
                        <div class="panel panel-default">
                            <div class="panel-heading qst">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                                       data-parent="#accordion"
                                       href="#collapse-<?php echo $i; ?>">
                                        <span
                                            class="date pull-right label label-info"><?php echo date('d m Y', $ask['date']); ?></span>
                                        <span class="name"><?php echo $ask['user']; ?></span>:
                                        <span class="title"><?php echo $ask['question']; ?></span>

                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-<?php echo $i; ?>" class="panel-collapse collapse">
                                <div class="panel-body ans">
                                    <div class="ans">
                                        <div class="name text-info h4"><?php echo $ask['manager']; ?> <span
                                                class="func label label-primary h5"><?php echo $ask['m_func']; ?></span>
                                        </div>

                                        <div class="title"><?php echo $ask['answer']; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <h3 id="formQuestRes">Задать вопрос</h3>
        <form id="formQuestion" class="row">
            <div class="col-xs-12 col-sm-6"><input name="type" value="Вопрос" type="hidden"> <input name="subject"
                                                                                                    value="question"
                                                                                                    type="hidden">
                <div class="form-group" own="name">
                    <error></error>
                    <input onchange="checkField('formQuestion')" class="form-control" name="name" value=""
                           placeholder="Ваше имя" type="text1"></div>
                <div class="form-group" own="phone">
                    <error></error>
                    <span class="help-block">Для получения ответа на телефон</span>
                    <input onchange="checkField('formQuestion')" class="form-control" name="phone" value=""
                           placeholder="Ваше телефон" type="text1"></div>
                <div class="form-group" own="email">
                    <error></error>
                    <span class="help-block">Для получения ответа на email</span>

                    <input onchange="checkField('formQuestion')" class="form-control" name="email" value=""
                           placeholder="Ваше e-mail" type="text1"></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group"  own="comment">
                    <error></error>
                    <textarea class="form-control" name="comment" rows="5"
                                                  placeholder=" Ваш вопрос"></textarea>
                    <span class="help-block">Если Вы правильно заполнили все поля, нажмите кнопку ниже:</span>
                </div>
                <button type="button"
                        onclick="AjaxResForm('formQuestRes', 'formQuestion', '/sender/send', 'Ваш вопрос отправлен, ответ будет отправлен по указанным Вами контактным данным. Также вы сможете увидеть его на данной странице сразу после ответа менеджера'); return false;"
                        class="btn btn-primary btn-lg">Спросить
                </button>
            </div>
        </form>
    </div>
</div>