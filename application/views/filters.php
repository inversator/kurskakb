<?php
if(count($units) > 0){
foreach ($units as $unit): ?>
    <div class='col-sm-6 col-md-4'>
        <div class='thumbnail unit'>
            <div class='unitListImg'>
                <a href='<?php echo "/".$url."/".$unit['url']; ?>'><img alt='' class='unitImg' src='/public/images/unit/<?php echo Room::showImg($unit['image']); ?>'></a>
            </div>
            <div class='caption unitListBlock'>
                <h4 class='unitListName'><a href='<?php echo "/".$url."/".$unit['url']; ?>'><?php echo $unit['pTitle']; ?></a></h4>
                <div class='unitListDesc'><?php echo Room::subDesc($unit['desc']); ?></div>
                <div class='unitButtons'>
                    <a role='button' class='btn btn-primary' onclick='addToCart(<?php echo $unit['id']; ?>);
                                return false;'>В корзину</a>
                       <?php if ($unit['price']): ?>
                        <div class='priceCatList'><?php echo $unit['price']; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; 
} else {?>
<p class='alert alert-warning'>Материалы не найдены</p>
<?php } ?>
