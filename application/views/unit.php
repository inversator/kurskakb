<div class="row row-offcanvas row-offcanvas-right">

    <div class="col-xs-12 col-sm-9">
        <p class="pull-right visible-xs">
            <button class="btn btn-primary btn-xs" data-toggle="offcanvas" type="button">Список категорий</button>
        </p>
        <div class="page-header">
            <h1><?php echo $title; ?></h1>
            <p class="lead"><?php echo $desc; ?></p>
        </div>
        <div class="row">
            <?php foreach ($units as $unit): ?>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <a href='/<?php echo $unit['url']; ?>'><img alt="" class="unitImg" src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a>
                        <div class="caption">
                            <h5><a href='/<?php echo $unit['url']; ?>'><?php echo $unit['pTitle']; ?></a></h5>
                            <p><?php echo Room::subDesc($unit['desc']); ?></p>
                            <div>
                                <a role="button" class="btn btn-primary" href="/<?php echo $unit['url']; ?>">В корзину</a>
                                <?php if($unit['price']): ?>
                                <div class="priceCap"></div>
                                <div class="priceCatList"><?php echo $unit['price']; ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="fullText"><?php echo $text; ?></div>
    </div>

    <div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
        <?php echo $rigthMenu; ?>
    </div>
</div>
<script>
$(document).ready(function () {
  $('[data-toggle=offcanvas]').click(function () {
    $('.row-offcanvas').toggleClass('active')
  });
});
</script>