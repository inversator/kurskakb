<div class="container" id="category">
    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button class="btn btn-primary btn-xs" data-toggle="offcanvas" type="button">Список категорий</button>
            </p>

            <div class="page-header">
                <h1><?php echo $title; ?></h1>

                <p class="lead"><?php echo htmlspecialchars_decode($desc); ?></p>
            </div>

            <?php if (count($units)): ?>
                <div class="row">
                    <?php if (isset($filters) && count($filters)): ?>
                        <nav role="navigation" class="navbar navbar-default navbar-static" id="navbar-filters">
                            <div class="container-fluid">
                                <div class="collapse navbar-collapse bs-example-js-navbar-collapse">
                                    <ul class="nav navbar-nav">
                                        <?php
                                        foreach ($filters as $filter): ?>
                                            <?php


                                            if (FALSE) {/* Пока ничего не делаем */
                                            } else {
                                                ?>
                                                <li class="dropdown">
                                                    <a data-toggle="dropdown" class="dropdown-toggle" role="button"
                                                       href="#"
                                                       id="drop<?php echo $filter['id']; ?>"><?php echo $filter['alias']; ?>
                                                        <b class="caret"></b></a>
                                                    <ul aria-labelledby="drop<?php echo $filter['id']; ?>" role="menu"
                                                        class="dropdown-menu filterBlock">
                                                        <?php
                                                        $i = 0;
                                                        foreach (array_unique(explode(',',
                                                            $filter['values'])) as $value):
                                                            ?>
                                                            <li class="filter" slug="<?php echo $slug; ?>"
                                                                cat="<?php echo $id; ?>">
                                                                <input type="checkbox"
                                                                       name="check[]"
                                                                       num="<?php echo $i; ?>"
                                                                       onchange="changeFilter();"
                                                                       id="<?php echo $filter['id']; ?>"
                                                                       value="<?php echo $value; ?>"><span class="name"><?php echo $value; ?></span>
                                                            </li>
                                                            <?php
                                                            $i++;
                                                        endforeach;
                                                        ?>
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    <?php endif; ?>
                    <?php if (!in_array($slug, array('news', 'article'))): ?>
                        <div id="sort" class="btn-group">
                            <button type="button" id="auto"
                                    class="btn btn-<?php echo Arr::get($_GET, 'order') == 'id' ? 'primary' : 'default' ?> btn-xs"
                                    onclick="applySort('order=id')">Автоматически
                            </button>
                            <button type="button" id="price"
                                    class="btn btn-<?php echo Arr::get($_GET, 'order') == 'price' ? 'primary' : 'default' ?> btn-xs"
                                    onclick="applySort('order=price')">По цене <span
                                    class="glyphicon glyphicon-chevron-up"></span></button>
                            <button type="button" id="price-desc"
                                    class="btn btn-<?php echo Arr::get($_GET, 'order') == 'price DESC' ? 'primary' : 'default' ?> btn-xs"
                                    onclick="applySort('order=price DESC')">По цене <span
                                    class="glyphicon glyphicon-chevron-down"></span></button>
                        </div>
                    <?php endif; ?>
                    <div class="paginationBlock"><?php echo $pagination; ?></div>
                    <hr>

                    <div class="filter"  type="category" slug="<?php echo $slug; ?>" cat="<?php echo $id; ?>"></div>
                    <div id="unitsBlock">
                        <?php foreach ($units as $unit): ?>
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail unit">
                                    <h4 class="unitListName">
                                        <?php if ($slug == 'news'): ?>
                                            <div class="newsDataLabel">
                                            <span class="label label-info">
                                                <?php echo date('d/m/Y', $unit['date']); ?>
                                            </span>
                                            </div>
                                        <?php endif; ?>
                                        <a href='<?php echo "/" . $url . "/" . $unit['url']; ?>'><?php echo $unit['pTitle']; ?></a>
                                    </h4>
                                    <div class="unitListImg">
                                        <a href='<?php echo "/" . $url . "/" . $unit['url']; ?>'><img alt=""
                                                                                                      class="unitImg"
                                                                                                      src="/public/images/unit/<?php
                                                                                                      if ($unit['image']) {
                                                                                                          echo 'thumbs/' . Room::showImg($unit['image']);
                                                                                                      } else {
                                                                                                          echo 'no-img.png';
                                                                                                      }
                                                                                                      ?>"></a>
                                    </div>
                                    <div class="caption unitListBlock">


                                        <div
                                            class="unitListDesc"><?php echo strip_tags(htmlspecialchars_decode(Room::subDesc($unit['desc'], 100))); ?></div>
                                        <div class="unitButtons">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php else: ?>
                <p class="alert alert-warning">В этой категории нет товаров</p>
            <?php endif; ?>

            <div class="paginationBlock"><?php echo $pagination; ?></div>
            <div class="fullText"><?php echo $text; ?></div>
        </div>

        <div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
            <?php echo $rigthMenu; ?>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            // Снимаем все чекбоксы
            $('.filter input:checkbox').prop('checked', false);
        })
    </script>
</div>