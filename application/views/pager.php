<?php
if (count($units) > 0) {
    foreach ($units as $unit):
        ?>
        <div class='row info '>
            <div class='infoListImg col-sm-3 col-xs-12'>
                <a href='<?php echo "/".$url."/".$unit['url']; ?>'><img alt='' class='unitImg' src='/public/images/unit/<?php echo Room::showImg($unit['image']); ?>'></a>
            </div>
            <div class='infoListBlock col-sm-9 col-xs-12'>
                <h4 class='infoListName'>
                    <span class="label label-info"><?php
                        echo date('d/m/Y', $unit['date']);
                        ?></span>
                    <a href='<?php echo "/".$url."/".$unit['url']; ?>'><?php echo $unit['pTitle']; ?></a>
                </h4>
                <div class='infoListDesc'><?php echo htmlspecialchars_decode($unit['desc']); ?></div>
                <div class="infoButtons pull-right">
                    <a href="<?php echo "/".$url."/".$unit['url']; ?>">Читать</a>
                </div>
            </div>
        </div>
        <?php
    endforeach;
} else {
    ?>
    <p class='alert alert-warning'>Материалы не найдены</p>
<?php } ?>
