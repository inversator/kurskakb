<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Административная панель</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo URL::base(); ?>bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo URL::base(); ?>public/js/main.js" type="text/javascript"></script>
        <link href="/public/css/main.css" rel="stylesheet">

    </head>

    <body>

        <div class="container">

            <form id="loginForm" role="form" class="form-signin" method="POST">
                <h2 class="form-signin-heading">Авторизация</h2>
                <div id="result_id"></div>

                <input name='username' type="text" autofocus="" required="" placeholder="Логин или email" class="form-control">
                <input name='password' type="password" required="" placeholder="Пароль" class="form-control">
                <!--            <label class="checkbox">
                                <input type="checkbox" value="remember-me"> Запомнить меню
                            </label>-->
                <button type="submit" class="btn btn-lg btn-primary btn-block" onclick="AjaxRegForm('result_id', 'loginForm', '/admin/login/check');
                        return false;">Войти</button>
            </form>

        </div> 
    </body>
</html>