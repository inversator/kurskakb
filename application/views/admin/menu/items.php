<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>

    <h2 class="sub-header">Список пунктов меню "<?php echo $menu['alias']; ?>" (<?php echo $menu['name']; ?> / id
        = <?php echo $menu['id']; ?>)</h2>

    <div id="result_id"></div>
    <div id="deleteResult" style="position: absolute;"></div>
    <div class="table-responsive">
        <?php if (count($items)) { ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя страницы</th>
                <th>Псевдоним</th>
                <th>Подпункты</th>
                <th>Избавиться</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $num = 1;

            foreach ($items as $item):
            ?>
            <tr id='ItemDel-<?php echo $item['id']; ?>'>
                <td><?php echo $num++; ?></td>
                <td><?php echo $item['alias']; ?></td>
                <td><?php echo $item['name']; ?></td>
                <td>
                    <button type="button" class="btn btn-default" data-toggle="collapse"
                            data-target=".demo-<?php echo $item['id']; ?>">Подпункты
                    </button>
                </td>
                <td>
                    <?php
                    if (!in_array($type, Kohana::$config->load('admin.menus'))):?>
                        <button
                            class="btn btn-danger"
                            onclick="DelObjectAjax('<?php echo $item['id']; ?>', '/admin/menu/delItem/', 'ItemDel-<?php echo $item['id']; ?>')
                                return true;" type="button">Удалить
                        </button>
                    <?php endif; ?>
                </td>
            </tr>
            <!-- Добавление пордпунктов меню START-->

            <?php if (count($item['child'])): ?>
                <tr id="" class="demo-<?php echo $item['id']; ?> collapse info">
                    <th>|</th>
                    <th>#</th>
                    <th>Имя страницы</th>
                    <th>Псевдоним</th>
                    <th>Подпункты</th>
                    <th>Избавиться</th>
                </tr>
                <?php foreach ($item['child'] as $child): ?>
                    <tr id='ItemDel-<?php echo $child['id']; ?>'  class="demo-<?php echo $item['id']; ?> collapse info">
                        <td>|____</td>
                        <td><?php echo $child['position']; ?></td>
                        <td><?php echo $child['alias']; ?></td>
                        <td><?php echo $child['name']; ?></td>
                        <td>
                            <button type="button" class="btn btn-default" data-toggle="collapse"
                                    data-target=".demo-<?php echo $item['id']; ?>">Подпункты
                            </button>
                        </td>
                        <td>
                            <button
                                class="btn btn-danger"
                                onclick="DelObjectAjax('<?php echo $item['id']; ?>', '/admin/menu/delItem/', 'ItemDel-<?php echo $child['id']; ?>')
                                    return true;" type="button">Удалить
                            </button>
                        </td>

                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            <tr id="" class="demo-<?php echo $item['id']; ?> collapse">
                <td colspan="5">
                    <div class="row-offcanvas">
                        <form id="addSubItem-<?php echo $item['id']; ?>">
                            <div class="col-md-2 vLabel">Добавить подПункт:</div>
                            <div class="col-md-5">
                                <?php if (count($pages)) { ?>
                                    <select class="form-control" name="page">
                                        <?php
                                        foreach ($pages as $page) {
                                            ?>
                                            <option
                                                value="<?php echo $page['id']; ?>"><?php echo $page['pTitle']; ?></option>
                                        <?php } ?>
                                    </select>
                                <?php } else { ?>
                                    <p class="bg-warning p15">-Страниц нет-</p>
                                <?php } ?>

                            </div>
                            <input type="hidden" name="type" value="<?php echo $item['id']; ?>"/>

                        </form>

                        <div class="col-md-5">
                            <button class="btn btn-success" type="button"
                                    onclick="AjaxFormRequest('result_id', 'addSubItem-<?php echo $item['id']; ?>', '/admin/menu/addSubItem');
                                    return true;">Добавить +
                            </button>
                        </div>

                    </div>
                </td>
            </tr>
    </div>
    <!-- Добавление пордпунктов меню END-->
    <?php endforeach;
    ?>
    </tbody>
    </table>
    <?php } else { ?>
        <p class="bg-warning p15">-Пунктов нет-</p>

    <?php } ?>

    <div class="row-offcanvas">
        <?php
        if (!in_array($type, Kohana::$config->load('admin.menus'))):
            ?>
            <form id="addForm">
                <div class="col-md-2 vLabel">Добавить пункт:</div>
                <div class="col-md-5">
                    <?php if (count($pages)) { ?>
                        <select class="form-control" name="page">
                            <?php
                            foreach ($pages as $page) {
                                ?>
                                <option value="<?php echo $page['id']; ?>"><?php echo $page['pTitle']; ?></option>
                            <?php } ?>
                        </select>
                    <?php } else { ?>
                        <p class="bg-warning p15">-Страниц нет-</p>
                    <?php } ?>

                </div>
                <input type="hidden" name="type" value="<?php echo $type; ?>"/>

            </form>

            <div class="col-md-5">
                <button class="btn btn-success" type="button"
                        onclick="AjaxFormRequest('result_id', 'addForm', '/admin/menu/addItem');
                                    return true;">Добавить +
                </button>
            </div>
        <?php endif; ?>
    </div>
</div>
</div>