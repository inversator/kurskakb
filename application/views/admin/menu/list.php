<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>

    <h2 class="sub-header">Список</h2>
    <div id="result_id"></div>
    <button class = "btn btn-success" type = "button" onclick="location.href = '/admin/menu/new/';
            return true;">Добавить +</button>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Имя страницы</th>
                    <th>Псевдоним</th>
                    <th>Пояснение</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $num = 1;
                foreach ($pages as $page):
                    ?>
                    <tr id='ItemDel-<?php echo $page['id']; ?>'>
                        <td><?php echo $num++; ?></td>
                        <td><a href='/admin/menu/edit/<?php echo $page['id']; ?>'><?php echo (!empty($page['alias']))
                        ? $page['alias'] : $page['name']; ?></a></td>

                        <td><?php echo $page['name']; ?></td>
                        <td class="grey">*<?php echo $page['comment']; ?></td>
                        <td><a href='/admin/menu/items/<?php echo $page['id']; ?>'>Редактировать пункты</a></td>
                        <td>
                            <?php
                            if (!in_array($page['id'],
                                    Kohana::$config->load('admin.menus'))):
                                ?>

                                <button 
                                    class="btn btn-danger" 
                                    onclick="DelObjectAjax('<?php echo $page['id']; ?>', '/admin/menu/delete/', 'ItemDel-<?php echo $page['id']; ?>')
                                                        return true;" type="button">Удалить меню</button>
    <?php endif; ?>
                        </td>
                    </tr>
<?php endforeach; ?>
            </tbody>
        </table>
        <?php echo isset($pagination) ? $pagination : ''; ?>
    </div>
</div>