<script type="text/javascript">
    var act = '<?php echo $act; ?>';

</script><div class="col-sm-9 col-md-10 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>

    <h2 class="sub-header"><?php echo (isset($page['name'])) ? $page['name'] : ''; ?></h2>

    <form role="form" id="pageForm">


        <div id="result_id"></div>
        <div class="row">
            <div class="col-xs-6">

                <div class="form-group">
                    <label>Название меню</label>
                    <input type="text" class="form-control" name="name" placeholder="Название меню" value="<?php echo (isset($page['name']))
        ? $page['name'] : ''; ?>">
                </div>


                <div class="form-group">
                    <label>Название меню на русском </label>
                    <input type="text" class="form-control" name="alias" placeholder="Название меню на русском" value="<?php echo (isset($page['alias']))
        ? $page['alias'] : ''; ?>">
                </div>




            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label>Комментарий</label>
                    <input type="text" class="form-control" name="comment" placeholder="Комментарий" value="<?php echo (isset($page['comment']))
        ? $page['comment'] : ''; ?>">
                </div>
                <div class="checkbox">
                    <label>
                        <input name="off" type="checkbox" <?php if (empty($page['off'])) echo "checked"; ?>> Опубликовано
                    </label>
                </div>

            </div>

        </div>
        <input type="hidden" name="id" value="<?php echo (isset($page['id'])) ? $page['id']
        : ''; ?>" />
        <button type="button" class="btn btn-default" onclick="AjaxFormRequest('result_id', 'pageForm', '/admin/menu/save');
                return true;">Сохранить</button>
    </form>


</div>