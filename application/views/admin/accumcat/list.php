<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>

    <h2 class="sub-header">Список</h2>

    <div id="result_id"></div>
    <button class="btn btn-success" type="button" onclick="location.href = '/admin/<?php echo $alias; ?>/new/';
            return true;">Добавить +
    </button>
    <div><?php echo isset($pagination) ? $pagination : ''; ?></div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Название категории</th>
                <th>Редактирование</th>
                <th>Удаление</th>
            </tr>
            </thead>
            <tbody>
            <?php echo Helper_Category::adminRecChildList($pages, $listNum); ?>
            </tbody>
        </table>
        <?php echo isset($pagination) ? $pagination : ''; ?>
    </div>
</div>