<script type="text/javascript">
    var act = '<?php echo $act; ?>';
</script>

<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>
    <h2 class="sub-header">#<?php echo isset($page['id']) ? $page['id'] : 'Неидентифицированный заказ'; ?></h2>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#fieldsTab" data-toggle="tab">Атрибуты заказа</a></li>
        <li><a href="#textTab" data-toggle="tab">Комментарий</a></li>
        <li><a href="#ordered" data-toggle="tab">Список заказанных товаров</a></li>
    </ul>

    <div class="ajax-respond"></div>
    <form role="form" id="pageForm" enctype="multipart/form-data">
        <div id="result_id"></div>

        <div class="tab-content">
            <div class="tab-pane active" id="fieldsTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Имя клиента</label>
                            <input type="text" class="form-control" name="name" placeholder="Название" value="<?php
                            echo (isset($page['name'])) ? $page['name'] : '';
                            ?>" disabled>
                        </div>
                        
                        <div class="form-group">
                            <label>Дата и время заказа</label>
                            <input type="text" class="form-control" name="datetime" placeholder="Дата и время заказа" value="<?php
                            echo (isset($page['datetime'])) ? $page['datetime'] : '';
                            ?>" disabled>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input name="call" type="checkbox" <?php
                                if ($page['call'] > 0) echo "checked";
                                ?> disabled> Перезвонить
                            </label>
                        </div>

                        <div class="form-group">
                            <label>Телефон</label>
                            <input type="text" class="form-control" name="phone" placeholder="Телефон" value="<?php
                            echo (isset($page['phone'])) ? $page['phone'] : '';
                            ?>" disabled>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" placeholder="email" value="<?php
                            echo (isset($page['email'])) ? $page['email'] : '';
                            ?>" disabled>
                        </div>

                        <div class="form-group">
                            <label>Индекс</label>
                            <input id="tinyDesc" class="form-control" name="index" placeholder="Не указан" value="<?php
                            echo (isset($page['index'])) ? $page['index'] : '';
                            ?>" disabled>
                            <p class="help-block">*Индекс города для доставки</p>
                        </div>

                        <div class="form-group">
                            <label>Город</label>
                            <input type="text" class="form-control" name="city" placeholder="не указан" value="<?php
                            echo (isset($page['city'])) ? $page['city'] : '';
                            ?>" disabled>
                        </div>

                        <div class="form-group">
                            <label>Адрес</label>
                            <textarea id="tinyText" class="form-control" name="address"><?php
                                echo (isset($page['address'])) ? $page['address']
                                        : '';
                                ?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Способ доставки</label>
                            <input type="text" class="form-control" name="delivery" placeholder="Не указан" value="<?php
                            echo (isset($page['delivery'])) ? KOHANA::$config->load('converter.delivery.'.$page['delivery'])
                                    : '';
                            ?>" disabled>
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane" id="textTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Комментарий клиента</label>
                            <textarea id="tinyText" class="fullText" class="form-control" name="comment" disabled><?php
                                echo (isset($page['comment'])) ? $page['comment']
                                        : '';
                                ?></textarea>
                        </div>

                    </div>
                </div>

            </div>

            <div class="tab-pane" id="ordered">
                <div class="row">
                    <div class="col-sm-12">
                        <table class='table'>

                            <?php foreach ($orderedProducts as $unit): ?>
                                <tr>
                                    <td><?php echo $unit['pTitle']; ?> <span class="badge right">x <?php echo $unit['count']; ?></span></td>
                                    <td><?php echo $unit['value'] * $unit['count']; ?></td>
                                </tr>
                            <?php endforeach; ?>

                            <tr><td>Итого</td><td><?php echo $sum; ?></td></tr>

                        </table>
                    </div>
                </div>

            </div>

        </div>

    </form>



</div>