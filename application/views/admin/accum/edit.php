<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>

    <h2 class="sub-header"><?php echo isset($page['pTitle']) ? $page['pTitle'] : 'Добавление аккумулятора'; ?></h2>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#fieldsTab" data-toggle="tab">Поля</a></li>
        <li><a href="#textTab" data-toggle="tab">Текст</a></li>
        <li><a href="#relationTab" data-toggle="tab">Подобные аккумуляторы</a></li>
        <li><a href="#relationTabCat" data-toggle="tab">Категории аккумулятора</a></li>
        <li><a href="#propertiesTab" data-toggle="tab">Свойства</a></li>
        <li><a href="#imagesTab" data-toggle="tab">Изображения</a></li>
        <li><a href="#filesTab" data-toggle="tab">Файлы</a></li>
    </ul>

    <div class="ajax-respond"></div>
    <form role="form" id="pageForm" enctype="multipart/form-data">
        <div id="result_id"></div>

        <div class="tab-content">
            <div class="tab-pane active" id="fieldsTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" class="form-control" name="pTitle" placeholder="Название" value="<?php
                            echo (isset($page['pTitle'])) ? $page['pTitle'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Заголовок в браузере</label>
                            <input type="text" class="form-control" name="bTitle" placeholder="Заголовок в браузере"
                                   value="<?php
                                   echo (isset($page['bTitle'])) ? $page['bTitle'] : '';
                                   ?>">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="off" type="checkbox" <?php if (empty($page['off'])) echo "checked"; ?>>
                                Опубликован
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="recommended"
                                       type="checkbox" <?php if (!empty($page['recommended'])) echo "checked"; ?>>
                                Рекомендуемый
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Ключевые слова</label>
                            <input type="text" class="form-control" name="meta_k" placeholder="Ключевые слова"
                                   value="<?php
                                   echo (isset($page['meta_k'])) ? $page['meta_k'] : '';
                                   ?>">
                        </div>

                        <div class="form-group">
                            <label>Описание для поисковых систем</label>
                            <input type="text" class="form-control" name="meta_d"
                                   placeholder="Описание для поисковых систем" value="<?php
                            echo (isset($page['meta_d'])) ? $page['meta_d'] : '';
                            ?>">
                        </div>
                        <div class="form-group">

                            <label>Дата создания</label>
                            <input id="datetimepicker" type="text" class="form-control" name="datetime" value="<?php
                            echo isset($page['date']) ? date('d.m.Y H:i',
                                $page['date']) : date('d.m.Y H:i');
                            ?>"/>

                            <p class="help-block">*Служит для сортировки старые/новые</p>
                        </div>
                        <div class="form-group">

                            <label>URL</label>
                            <input type="text" class="form-control" name="url" placeholder="url" value="<?php
                            echo (isset($page['url'])) ? $page['url'] : '';
                            ?>">

                            <p class="help-block">*Название страницы в адресной строке</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormUnit('result_id', 'pageForm', '/admin/accum/save');
                        return true;">Сохранить
                </button>
            </div>

            <div class="tab-pane" id="textTab">
                <br>
                <button type="button" class="btn btn-success" onclick="AjaxFormUnit('result_id', 'pageForm', '/admin/accum/save');
                        return true;">Сохранить
                </button>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Краткое описание</label>
                            <textarea id="tinyDesc" class="form-control mceEditor" name="desc"><?php
                                echo (isset($page['desc'])) ? $page['desc'] : '';
                                ?></textarea>

                            <p class="help-block">*Используется при выводе страниц в списке и как верхний блок на
                                странице с материалом</p>
                        </div>

                        <div class="form-group">
                            <label>Текст</label>
                            <textarea id="tinyText" class="fullText mceEditor" class="form-control" name="text"><?php
                                echo (isset($page['text'])) ? $page['text'] : '';
                                ?></textarea>

                            <p class="help-block">*Выводится на странице с материалом</p>
                        </div>

                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormUnit('result_id', 'pageForm', '/admin/accum/save');
                        return true;">Сохранить
                </button>
            </div>

            <div class="tab-pane" id="propertiesTab">
                <h3>Свойства</h3>
                <?php if (isset($unitProp)): ?>
                    <table class="table table-striped">
                        <thead>
                        <th>Порядок</th>
                        <th>Название свойства</th>
                        <th>Значение</th>
                        </thead>
                        <tbody id="tBody">
                        <?php
                        if (count($unitProp)):
                            foreach ($unitProp as $prop):
                                ?>

                                <tr id="ItemDel-<?php echo $prop['id']; ?>">
                                    <td><input class="form-control propPos"
                                               onchange="changePos(<?php echo $prop['id']; ?>)"
                                               id="pos<?php echo $prop['id']; ?>" value="<?php echo $prop['pos']; ?>"/>
                                    </td>
                                    <td><?php echo $prop['alias']; ?></td>
                                    <td><?php echo $prop['value']; ?></td>
                                    <td>
                                        <button type="button"
                                                onclick="DelObjectAjax(<?php echo $prop['id']; ?>, '/admin/accum/deleteProp/', 'ItemDel-<?php echo $prop['id']; ?>')
                                                    return true;" class="btn btn-danger">Удалить
                                        </button>
                                    </td>
                                </tr>

                                <?php
                            endforeach;
                        else:
                            ?>
                            <div class="alert alert-warning fade in">Свойства не заданы</div>
                        <?php endif; ?>
                        </tbody>
                    </table>

                    <form id="formProp" class="row">
                        <div class="col-xs-5">
                            <?php
                            echo Form::select('properties', $properties, 1,
                                array('class' => 'form-control', 'onchange' => 'LoadInput()',
                                    'id' => 'propSelect'));
                            ?>
                        </div>
                        <div class="col-xs-5" id="inputId"></div>
                        <div class="col-xs-2">
                            <button type="button" onclick="addPropery()" class="btn btn-success">Добавить+</button>
                        </div>
                    </form>
                <?php else: ?>
                    <p class="alert alert-warning fade in">* Для добавления свойств сохраните материал и перейдите к
                        редактированию</p>
                <?php endif; ?>
            </div>

            <div class="tab-pane" id="relationTab">
                <?php if (isset($list)): ?>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Имя материала</th>
                            <th>URL</th>
                            <th>Связать</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($list as $item): if ($item['id'] == $page['id']) {
                            continue;
                        }
                            ?>
                            <tr>
                                <td><?php echo $item['pTitle']; ?></td>
                                <td><?php echo $item['url']; ?></td>
                                <td><input <?php

                                    if (in_array($item['id'], $parList)) {
                                        echo "checked";
                                    }
                                    ?> class="parentCheck<?php echo $item['id']; ?>-unit" type="checkbox"
                                       onchange="parentRec(<?php echo $item['id'] . ", " . $page['id']; ?>, 'unit', 'unit');"/>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте
                        внимательны!), нажимать сохранить не обязательно.</p>
                <?php else: ?>
                    <p class="help-block">*Для создания связей сохраните страницу</p>
                <?php endif; ?>
            </div>

            <div class="tab-pane" id="relationTabCat">
                <?php if (isset($list)): ?>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Имя категории</th>
                            <th>URL</th>
                            <th>Назначить родительской</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php echo Helper_Category::adminRecChild($catList, $parCatList, '', $page); ?>

                        </tbody>
                    </table>
                    <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте
                        внимательны!), нажимать сохранить не обязательно.</p>
                <?php else: ?>
                    <p class="help-block">*Для создания связей сохраните страницу</p>
                <?php endif; ?>
            </div>

            <div class="tab-pane" id="imagesTab">
                <h3>Изображения</h3>

                <?php if (isset($page['id'])): ?>
                    <div class="form-group">
                        <div class="imgConteiner">
                            <?php
                            if (!isset($images) || !count($images)):
                                ?>
                                <img data-toggle="modal" data-target="#myModal" src="/public/images/unit/no-img.png"
                                     alt="Нет изображения" class="img-thumbnail pointer"/>
                            <?php else: ?>
                                <?php foreach ($images as $image): ?>
                                    <div id="imgBlock-<?php echo $image['id']; ?>" class="imgBlock img-thumbnail">

                                        <img class="imgTeg" src="/public/images/unit/<?php echo $image['name']; ?>"/>

                                        <div class="imgControl">
                                            <div class="imgDel">
                                                <div url="/admin/unit/delImage" idimg="<?php echo $image['id']; ?>"><a>Удалить</a>
                                                </div>
                                            </div>
                                            <div class="imageCheck">
                                                <input id_img="<?php echo $image['id']; ?>"
                                                       id_es="<?php echo $image['id_es']; ?>" <?php if ($image['main']) echo 'checked'; ?>
                                                       type="radio" name="mainImg" data-toggle="tooltip"
                                                       data-placement="top" title="Выбрать главным"/>
                                            </div>
                                        </div>

                                    </div>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </div>

                        <hr>

                        <button type="button" id="addImg" class="btn btn-primary" data-toggle="modal"
                                data-target="#myModal">
                            Добавить
                        </button>

                        <!-- Модаль изображений -->

                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div id="modal_result_id"></div>
                                        <h4 class="modal-title" id="myModalLabel">Добавить изображение</h4>
                                    </div>
                                    <div class="modal-body">
                                        <label for="exampleInputFile">Изображение аккумулятора</label>
                                        <input name="img" type="file" id="inputFile">

                                        <p class="help-block">*Допустимые форматы PNG, JPG</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                        <button id="uploadFile" essence="unit" type="button" class="btn btn-primary">
                                            Загрузить файл
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте
                            внимательны!), нажимать сохранить не обязательно.</p>
                    </div>
                <?php else: ?>
                    <p class="alert alert-warning fade in">* Для добавления изображений сохраните данные и перейдите к
                        редактированию</p>
                <?php endif; ?>
            </div>

            <div class="tab-pane" id="filesTab">
                <h3>Прикрепленные файлы</h3>
                <?php if (isset($page['id'])): ?>
                    <table class="table table-strepped" id="fileContainer">
                        <tr>
                            <td>Тип</td>
                            <td>Название</td>
                        </tr>
                        <?php foreach ($docs as $doc):
                            ?>
                            <tr id="fileBlock-<?php echo $doc->id; ?>">
                                <td width="100"><img width="50" src="/public/images/<?php echo $doc->type; ?>.png"
                                                     title="<?php echo $doc->type; ?>"
                                                     alt="<?php echo $doc->type; ?>"
                                        /></td>
                                <td align="left" style="vertical-align: middle;"><a target="_blank"
                                                                                    href="/public/files/<?php echo $doc->file; ?>">
                                        <strong><?php echo $doc->file; ?></strong></a></td>
                                <td fileid="<?php echo $doc->id; ?>" class="changeFile" data-toggle="modal"
                                    data-target="#fileDesc"><span class="btn btn-default">Описание</span>
                                </td>
                                <td class="fileDel" url="/admin/unit/delFile" idfile="<?php echo $doc->id; ?>"><a>Удалить</a>
                                </td>
                                <td style="display: none"
                                    id="descFile-<?php echo $doc->id; ?>"><?php echo $doc->description; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <hr>
                    <button type="button" id="addImg" class="btn btn-primary" data-toggle="modal"
                            data-target="#filesModal">
                        Добавить
                    </button>
                <?php else: ?>
                    <p class="alert alert-warning fade in">* Для добавления файлов сохраните данные и перейдите к
                        редактированию</p>
                <?php endif; ?>
                <!-- Модаль файлов -->

                <div class="modal fade" id="filesModal" tabindex="-1" role="dialog" aria-labelledby="filesModal"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div id="modal_result_id_file"></div>
                                <h4 class="modal-title" id="myModalLabel">Добавить файл</h4>
                            </div>
                            <div class="modal-body">
                                <label for="exampleInputFile">Документы к аккумулятору</label>
                                <input name="file" type="file" id="inputFile">

                                <p class="help-block">*Допустимые форматы PDF, DOC</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                </button>
                                <button id="uploadFile2" essence="unit" type="button" class="btn btn-primary">
                                    Загрузить файл
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <input type="hidden" name="id" value="<?php
        echo (isset($page['id'])) ? $page['id'] : '';
        ?>"/>
    </form>

    <div class="modal fade" id="fileDesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Описание файла (название на русском)</h4>
                </div>
                <div class="modal-body">
                    <div id="result"></div>
                    <input type="hidden" value="" name="id_file"/>
                    <textarea name="desc" type="text" class="form-control" id="fileDescField"/></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary" onclick="descFile()">Сохранить изменения</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#fileDesc').on('show.bs.modal', function (e) {
            var idFile = $(e.relatedTarget).attr('fileid');
            $("input[name='id_file']").val(idFile);
            $('#fileDesc textarea').val($('#descFile-' + idFile).text());

        })
    </script>

</div>
<script type="text/javascript">
    var act = '<?php echo $act; ?>';
    LoadInput();
    jQuery('#datetimepicker').datetimepicker({
        format: 'd.m.Y H:i'
    });</script>