<script type="text/javascript">
    var act = '<?php echo $act; ?>';
</script>

<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>

    <h2 class="sub-header"><?php echo isset($page['pTitle']) ? $page['pTitle'] : 'Новая страница'; ?></h2>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#fieldsTab" data-toggle="tab">Поля</a></li>
        <li><a href="#textTab" data-toggle="tab">Текст</a></li>
        <li><a href="#relationTab" data-toggle="tab">Родительские страницы</a></li>
    </ul>

    <div class="ajax-respond"></div>
    <form role="form" id="pageForm" enctype="multipart/form-data">
        <div id="result_id"></div>


        <div class="tab-content">
            <div class="tab-pane active" id="fieldsTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" class="form-control" name="pTitle" placeholder="Название" value="<?php
                            echo (isset($page['pTitle'])) ? $page['pTitle'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Заголовок в браузере</label>
                            <input type="text" class="form-control" name="bTitle" placeholder="Заголовок в браузере"
                                   value="<?php
                                   echo (isset($page['bTitle'])) ? $page['bTitle'] : '';
                                   ?>">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="off" type="checkbox" <?php if (empty($page['off'])) echo "checked"; ?>>
                                Опубликована
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Ключевые слова</label>
                            <input type="text" class="form-control" name="meta_k" placeholder="Ключевые слова"
                                   value="<?php
                                   echo (isset($page['meta_k'])) ? $page['meta_k'] : '';
                                   ?>">
                        </div>

                        <div class="form-group">
                            <label>Описание для поисковых систем</label>
                            <input type="text" class="form-control" name="meta_d"
                                   placeholder="Описание для поисковых систем" value="<?php
                            echo (isset($page['meta_d'])) ? $page['meta_d'] : '';
                            ?>">
                        </div>
                        <div class="form-group">
                            <label>Краткое описание</label>
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                          Показать/скрыть
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <textarea id="tinyDesc" class="form-control mceEditor" name="desc">
                                                <?php echo (isset($page['desc'])) ? $page['desc'] : ''; ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <p class="help-block">*Используется при выводе страниц в списке и как верхний блок на
                                странице с материалом</p>
                        </div>

                        <div class="form-group">

                            <label>URL</label>
                            <input type="text" class="form-control" name="url" placeholder="url" value="<?php
                            echo (isset($page['url'])) ? $page['url'] : '';
                            ?>">

                            <p class="help-block">*Название страницы в адресной строке</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormPage('result_id', 'pageForm', '/admin/page/save');
                        return true;">Сохранить
                </button>
            </div>

            <div class="tab-pane" id="textTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Текст</label>
                            <textarea id="tinyText" class="fullText mceEditor" class="form-control" name="text"><?php
                                echo (isset($page['text'])) ? $page['text'] : '';
                                ?></textarea>

                            <p class="help-block">*Выводится на странице с материалом</p>
                        </div>

                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormPage('result_id', 'pageForm', '/admin/page/save');
                        return true;">Сохранить
                </button>
            </div>

            <div class="tab-pane" id="relationTab">
                <?php if (isset($list)): ?>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Имя страницы</th>
                            <th>URL</th>
                            <th>Назначить родительской</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach ($list as $item): if ($item['id'] == $page['id']) {
                            continue;
                        }
                            ?>
                            <tr>

                                <td><?php echo $item['pTitle']; ?></td>
                                <td><?php echo $item['url']; ?></td>
                                <td><input <?php

                                    if (in_array($item['id'], $parList)) {
                                        echo "checked";
                                    }
                                    ?>
                                        class="parentCheck<?php echo $item['id']; ?>-page"
                                        id="parentCheck<?php echo $item['id']; ?>" type="checkbox"
                                        onchange="parentRec(<?php echo $item['id'] . ", " . $page['id']; ?>, 'page', 'page');"/>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте
                        внимательны!), нажимать сохранить не обязательно.</p>
                <?php else: ?>
                    <p class="help-block">*Для создания связей сохраните страницу</p>
                <?php endif; ?>
            </div>

        </div>

        <input type="hidden" name="id" value="<?php
        echo (isset($page['id'])) ? $page['id'] : '';
        ?>"/>
    </form>


</div>