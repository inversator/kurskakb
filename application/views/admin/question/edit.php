<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header">Вопрос: ответ</h1>

    <h4 class="sub-header"><?php echo isset($page['question']) ? $page['question'] : 'Добавление вопроса'; ?></h4>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#questionTab" data-toggle="tab">Вопрос</a></li>
        <li><a href="#answerTab" data-toggle="tab">Ответ</a></li>
    </ul>

    <div class="ajax-respond"></div>
    <form role="form" id="pageForm" enctype="multipart/form-data">
        <div id="result_id"></div>

        <div class="tab-content">
            <div class="tab-pane active" id="questionTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Пользователь</label>
                            <input type="text" class="form-control" name="user" placeholder="Название" value="<?php
                            echo (isset($page['user'])) ? $page['user'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Дата создания</label>
                            <input id="datetimepicker" type="text" class="form-control" name="datetime" value="<?php
                            echo isset($page['date']) ? date('d.m.Y H:i',
                                $page['date']) : date('d.m.Y H:i');
                            ?>"/>

                            <p class="help-block">*Служит для сортировки старые/новые</p>
                        </div>
                        <div class="form-group">
                            <label>Вопрос</label>
                            <textarea id="tinyDesc" class="form-control mceEditor" name="question"><?php
                                echo (isset($page['question'])) ? $page['question'] : ''; ?>
                            </textarea>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success" onclick="AjaxForm('result_id', 'pageForm', '/admin/question/save');
                        return true;">Сохранить
                    </button>
                </div>
            </div>

            <div class="tab-pane" id="answerTab">
                <br>
                <button type="button" class="btn btn-success" onclick="AjaxForm('result_id', 'pageForm', '/admin/question/save');
                    return true;">Сохранить
                </button>
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Менеджер</label>
                            <input type="text" class="form-control" name="manager" placeholder="Менеджер" value="<?php
                            echo (isset($page['manager'])) ? $page['manager'] : '';
                            ?>">
                        </div>


                        <div class="form-group">
                            <label>Должность менеджера</label>
                            <input type="text" class="form-control" name="m_func" placeholder="Должность менеджера"
                                   value="<?php
                                   echo (isset($page['m_func'])) ? $page['m_func'] : '';
                                   ?>">
                        </div>
                        
                        <div class="form-group">
                            <label>Ответ</label>
                            <textarea id="tinyText" class="fullText mceEditor" class="form-control" name="answer"><?php
                                echo (isset($page['answer'])) ? $page['answer'] : '';
                                ?>
                            </textarea>

                            <p class="help-block">*Выводится на странице с материалом</p>
                        </div>

                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxForm('result_id', 'pageForm', '/admin/question/save');
                        return true;">Сохранить
                </button>
            </div>
        </div>

        <input type="hidden" name="id" value="<?php
        echo (isset($page['id'])) ? $page['id'] : '';
        ?>"/>
    </form>


</div>
<script type="text/javascript">
    var act = '<?php echo $act; ?>';
    LoadInput();
    jQuery('#datetimepicker').datetimepicker({
        format: 'd.m.Y H:i'
    });
    function AjaxForm(result_id, form_id, url) {
        var data = {
            'question': tinyMCE.get('tinyDesc').getContent(),
            'answer': tinyMCE.get('tinyText').getContent(),
            'user': $("input[name='user']").val(),
            'manager': $("input[name='manager']").val(),
            'm_func': $("input[name='m_func']").val(),
            'date': ToTomestamp($("input[name='datetime']").val()),
            'id': $("input[name='id']").val()
        };

        jQuery.ajax({
            url: url, //Адрес подгружаемой страницы
            type: "POST", //Тип запроса
            dataType: "html", //Тип данных
            data: data,
            success: function (response) { //Если все нормально
                document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
//            if (act == 'new') {
//                jQuery('form')[0].reset();
//            }
            },
            error: function (response) { //Если ошибка
                document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
            }
        });
    }
</script>
