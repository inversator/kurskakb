
<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>

    <div class="row placeholders">
        <div class="col-xs-6 col-sm-3 placeholder">
            <img data-src="" class="img-responsive" alt="Версия CMS ROOM">
            <h4>2.0</h4>
            <span class="text-muted">beta</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
            <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Версия языка PHP">
            <h4><?php echo phpversion() ?></h4>
            <span class="text-muted">Требуемая от 5.3</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
            <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Свободного места на диске">
            <h4><?php echo Room::dataSize(disk_free_space(".")); ?></h4>
            <span class="text-muted">ИЗ <?php echo Room::dataSize(disk_total_space(".")); ?></span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
            <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Версия Mysql">
            <h4></h4>
            <span class="text-muted">Разрабатывалась на 5.5.4</span>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs12 h2">Контакты разработчика</div>

        <div class="col-xs-12 h3"><a href="mailto:aleksey.markov.msk@gmail.com">aleksey.markov.msk@gmail.com</a></div>
        <div class="col-xs-12 h3">тел. +7 (920) 632-07-29</div>
    </div>
</div>