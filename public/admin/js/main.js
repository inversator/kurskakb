function AjaxFormRequest(result_id, form_id, url) {

    jQuery.ajax({
        url: url, //Адрес подгружаемой страницы
        type: "POST", //Тип запроса
        dataType: "html", //Тип данных
        data: jQuery("#" + form_id).serialize(),
        success: function (response) { //Если все нормально
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
//            if (act == 'new') {
//                jQuery('form')[0].reset();
//            }
        },
        error: function (response) { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}

function AjaxRequest(result_id, id, url, block) {
    jQuery.ajax({
        url: url, //Адрес подгружаемой страницы
        type: "POST", //Тип запроса
        dataType: "html", //Тип данных
        data: {id}, //
        success: function (response) { //Если все нормально
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        },
        error: function (response) { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
    if (block) {
        $('#' + block).remove();
    }
    $('#result_div_id').remove();
    $('#windowModal').remove();
}

function DelObjectAjax(id, url, block) {
    result_id = 'result_id';
    quote = "'";
    text = '<div id="windowModal" class="absolute"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button onclick="closeModal()" class="close" type="button">×</button>';
    text += '<h4 class="modal-title">Удаление элемента</h4></div><div class="modal-body"><p>Вы уверены?</p></div>';
    text += '<div class="modal-footer"><button data-dismiss="modal" class="btn btn-default" onclick="closeModal()" type="button">Закрыть</button>';
    text += '<button class="btn btn-primary" onclick="AjaxRequest(' + quote + result_id + quote + ', ' + quote + id + quote + ',' + quote + url + quote + ',' + quote + block + quote + ')" type="button">Удалить</button></div></div></div></div>';
    $('body').append(text);
}

function closeModal() {
    $('#windowModal').remove();
}

function AjaxLoginForm(result_id, form_id, url) {
    jQuery.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: jQuery("#" + form_id).serialize(),
        success: function (response) {

            if (!response[0]) {
                document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response[1] + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
            } else {
                window.location.replace(response[1]);
            }
        },
        error: function () { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}

function AjaxFormPage(result_id, form_id, url) {

    var data = {
        'desc': tinyMCE.get('tinyDesc').getContent(),
        'text': tinyMCE.get('tinyText').getContent(),
        'pTitle': $("input[name='pTitle']").val(),
        'bTitle': $("input[name='bTitle']").val(),
        'off': $("input[name='off']").prop('checked'),
        'meta_k': $("input[name='meta_k']").val(),
        'meta_d': $("input[name='meta_d']").val(),
        'url': $("input[name='url']").val(),
        'id': $("input[name='id']").val()
    };
    jQuery.ajax({
        url: url, //Адрес подгружаемой страницы
        type: "POST", //Тип запроса
        dataType: "html", //Тип данных
        data: data,
        success: function (response) { //Если все нормально
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
//            if (act == 'new') {
//                jQuery('form')[0].reset();
//                tinymce.get('tinyDesc').setContent('');
//                tinymce.get('tinyText').setContent('');
//            }
        },
        error: function (response) { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}

/* Функция превращения времени в TimeStamp 
 * в формате 'd.m.Y H:i'
 * */

function ToTomestamp(datetime) {
    if (datetime) {
        var dateString = datetime,
            dateParts = dateString.split(' '),
            timeParts = dateParts[1].split(':'),
            date;
        dateParts = dateParts[0].split('.');
        dateTimeBlock = new Date(dateParts[2], parseInt(dateParts[1], 10) - 1, dateParts[0], timeParts[0], timeParts[1]);
        date = dateTimeBlock.getTime();
        date = date.toString();
        date = date.substring(0, date.length - 3);
    } else {
        date = jQuery.now().substring(0, jQuery.now().length - 3);
    }

    return date;
}

function AjaxFormUnit(result_id, form_id, url) {

    var data = {
        'desc': tinyMCE.get('tinyDesc').getContent(),
        'text': tinyMCE.get('tinyText').getContent(),
        'pTitle': $("input[name='pTitle']").val(),
        'bTitle': $("input[name='bTitle']").val(),
        'off': $("input[name='off']").prop('checked'),
        'recommended': $("input[name='recommended']").prop('checked'),
        'meta_k': $("input[name='meta_k']").val(),
        'meta_d': $("input[name='meta_d']").val(),
        'url': $("input[name='url']").val(),
        'id': $("input[name='id']").val(),
        'date': ToTomestamp($("input[name='datetime']").val())
    };
    jQuery.ajax({
        url: url, //Адрес подгружаемой страницы
        type: "POST", //Тип запроса
        dataType: "html", //Тип данных
        data: data,
        success: function (response) { //Если все нормально
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
//            jQuery('form')[0].reset();
//            tinymce.get('tinyDesc').setContent('');
//            tinymce.get('tinyText').setContent('');
        },
        error: function (response) { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}


function delImg(block) {

    var data = {
        'id': $(block).attr('idimg')
    };
    jQuery.ajax({
        url: $(block).attr('url'),
        type: "POST",
        dataType: "html",
        data: data,
        success: function (response) {
            if (response) {
                $('#imgBlock-' + data['id']).remove();
            }
        },
        error: function () {
            console.log('ошибка')
        }
    });
}

function delFile(block) {

    var data = {
        'id': $(block).attr('idfile')
    };
    jQuery.ajax({
        url: $(block).attr('url'),
        type: "POST",
        dataType: "html",
        data: data,
        success: function (response) {
            if (response) {
                $('#fileBlock-' + data['id']).remove();
            }
        },
        error: function () {
            console.log('ошибка')
        }
    });
}

function parentRec(parRec, dotRec, parEss, dotEss) {
    if ($('.parentCheck' + parRec + '-' + parEss).prop("checked")) {
        $.get("/admin/" + parEss + "/addParent",
            {
                par_rec: parRec,
                dot_rec: dotRec,
                par_ess: parEss,
                dot_ess: dotEss
            }
        );
    } else {

        $.get("/admin/" + parEss + "/delParent",
            {
                par_rec: parRec,
                dot_rec: dotRec,
                par_ess: parEss,
                dot_ess: dotEss
            }
        );
    }
}
/*
 * Сделать изображение главным для сущности
 */
function mainImg(block) {
    $.get(
        "/image/main",
        {
            id_img: $(block).attr('id_img'),
            id_es: $(block).attr('id_es')
        }
    );
}

/*
 * Задать описание изображению
 */
function descImg() {
    $.get(
        "/image/desc",
        {
            id_img: $("input[name='id_img']").val(),
            desc_img: $('#imageDesc textarea').val()
        }
    ).success(function () {
            var html = '<div class="alert alert-warning fade in">' +
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                '<strong>Изменения сохранены!</strong></div>';
            $('#result').html(html);
            $(".alert").fadeOut(2000, function () {
                $(".alert").alert('close');
            });

        })
    ;
}

/*
 * Set file Description
 */

function descFile() {

    var id = $("input[name='id_file']").val();
    var desc = $('#fileDesc textarea').val();
    $.get(
        "/file/desc",
        {
            id_file: id,
            desc_file: desc
        }
    ).success(function () {
            var html = '<div class="alert alert-warning fade in">' +
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                '<strong>Изменения сохранены!</strong></div>';
            $('#result').html(html);
            $('#descFile-' + id).text(desc);
            $(".alert").fadeOut(2000, function () {
                $(".alert").alert('close');
            });

        })
    ;
}

function AddValue(result_id, form_id) {
    jQuery.ajax({
        url: '/admin/property/addValue', //Адрес подгружаемой страницы
        type: "POST", //Тип запроса
        dataType: "html", //Тип данных
        data: jQuery("#" + form_id).serialize(),
        success: function (response) { //Если все нормально
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
            jQuery('#' + form_id)[0].reset();
            $.get(
                "/admin/property/getFixedValues",
                {
                    id_prop: $('input[name=id]').val()
                },
                function (data) {
                    var html = '';
                    var num = 0;
                    var x = data.length;
                    if (x > 0) {
                        while (x--) {
                            num++;
                            html += "<tr id='ItemDel-" + data[x]['id'] + "'>";
                            html += "<td>" + num + "</td>";
                            html += "<td>" + data[x]['value'] + "</td>"
                            html += "<td><button  class='btn btn-danger'";
                            html += 'onclick="';
                            html += "DelObjectAjax(" + data[x]['id'] + ", '/admin/property/delFixedValue/', 'ItemDel-" + data[x]['id'] + "'); return true;";
                            html += '" type="button">Удалить</button>';
                            html += '</td></tr>';
                        }

                        $('.table').html(html);
                        $('#noValue').remove();
                        $('.Del div').click(function () {
                            delImg(this);
                        });
                    }
                }, "json");
        },
        error: function (response) { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}

function LoadInput() {
    var idProp = $('#propSelect').val();
    $.get("/admin/property/getType",
        {id: idProp},
        function (data) {

            var html = '';
            if (data > 0) {
                $.get("/admin/property/getFixedValues",
                    {id_prop: idProp},
                    function (data) {
                        var x = data.length;
                        if (x > 0) {
                            html += '<select id="propValue" class="form-control">';
                            while (x--) {

                                html += '<option value="' + data[x]['value'] + '">' + data[x]['value'] + '</option>';
                            }
                            html += '</select>';
                            $('#inputId').html(html);
                        } else {
                            html += '<div class="alert alert-warning fade in">Не заданы значения свойства</div>'
                            $('#inputId').html(html);
                        }
                    }, "json");
                $('#inputId').html(html);
            } else {
                html += '<input type="text" id="propValue" class="form-control" />';
                $('#inputId').html(html);
            }
        }
    );
}

function changePos(id) {
    var pos = $('#pos' + id).val();

    $.get("/admin/unit/changePos",
        {
            pos: pos,
            id: id
        },
        function () {
            updateProp($('input[name=id]').val());
        }
    );
}

function addPropery() {
    var prop = $('#propSelect').val();
    var value = $('#propValue').val();
    var unit = $('input[name=id]').val();
    if (prop && value) {

        $.get(
            "/admin/unit/addProperty",
            {
                id_prop: prop,
                value: value,
                id_unit: unit
            },
            function (data) {
                if (data[1] > 0) {
                    updateProp(unit);
                }
            }, "json");
    }
}

function updateProp(unit) {
    $.get("/admin/unit/getProperties",
        {id: unit},
        function (data) {
            var x = data.length;
            var html = '';
            if (x > 0) {
                while (x--) {

                    html += '<tr id="ItemDel-' + data[x]['id'] + '">';
                    html += '<td><input class="form-control propPos" onchange="changePos(' + data[x]['id'] + ')" id="pos' + data[x]['id'] + '" value="' + data[x]['pos'] + '" /></td>';
                    html += '<td>' + data[x]['alias'] + '</td>';
                    html += '<td>' + data[x]['value'] + '</td>';
                    html += '<td><button type="button" onclick="DelObjectAjax(' + data[x]['id'];
                    html += ",'/admin/unit/deleteProp/', 'ItemDel-" + data[x]['id'] + "');";
                    html += ' return true;" class="btn btn-danger">Удалить</button></td>';
                    html += '</tr>';
                }
            }
            $('#tBody').html(html);
        }, "json"
    );
}

/* Инициализация */
$(document).ready(function () {

// Запускаем пикер
    jQuery.datetimepicker.setLocale('ru');
    // Всплывающие подсказки для всех элементов
    $('[data-toggle="tooltip"]').tooltip();
    // Обнуляем поле файла
    $('button#addImg').click(function () {
        $('input[type=file]').val('');
    });
    // Получение файла из формы
    var files;
    $('input[type=file]').change(function () {
        files = this.files;
    });
    // Задаем главное изображение
    $('input[name=mainImg]').change(function () {
        mainImg(this);
    })

    // Удаление изображения
    $('.imgDel div').click(function () {
        delImg(this);
    });

    $('.fileDel').click(function () {
        delFile(this);
    });

    //Загрузка файла сущности
    $('#uploadFile').click(function (event) {

// Создадим данные формы и добавим в них данные файлов из files
        if (files !== undefined) {

            var data = new FormData();
            $.each(files, function (key, value) {
                data.append(key, value);
            });
            var essence = $(this).attr('essence');
            //Добаваляем id сущности
            data.append('id_es', $('input[name=id]').val());
            // Отправляем запрос

            $.ajax({
                url: '/admin/' + essence + '/addImage',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Не обрабатываем файлы (Don't process the files)
                contentType: false, // Так jQuery скажет серверу что это строковой запрос
                success: function (respond) {
                    $('#modal_result_id').html("<div class='alert alert-warning fade in' id='result_div_id'>" + respond + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>");
                    $.get(
                        "/image/get",
                        {
                            essence: essence,
                            id_es: $('input[name=id]').val()
                        },
                        function (data) {
                            var html = '';
                            var x = data.length;
                            if (x) {
                                while (x--) {
                                    var checked = '';
                                    if (data[x]['main'])
                                        checked = 'checked';
                                    html += '<div id="imgBlock-' + data[x]['id'] + '" class="imgBlock img-thumbnail">';
                                    html += ' <img class="imgTeg" src="/public/images/' + essence + '/' + data[x]['name'] + '" />';
                                    html += '<div class="imgControl">';
                                    html += '<div class="imgDel">';
                                    html += '<div url="/admin/' + essence + '/delImage" idimg="' + data[x]['id'] + '"><a>Удалить</a></div>';
                                    html += '</div>';
                                    html += '<div class="imageCheck">';
                                    html += '<input id_img="' + data[x]['id'] + '" id_es="' + data[x]['id_es'] + '" type="radio" ' + checked + '  name="mainImg" data-toggle="tooltip"  data-placement="top" title="Выбрать главным" />';
                                    html += '</div></div></div>';
                                }

                                $('div.imgConteiner').html(html);
                                $('.imgDel div').click(function () {
                                    delImg(this);
                                });
                                $('input[name=mainImg]').change(function () { // Задаем главное изображение
                                    mainImg(this);
                                })
                            }
                        }, "json");
                    $('input[type=file]').val('');
                }
            });
        } else {

            $('#modal_result_id').html("<div class='alert alert-warning fade in' id='result_div_id'>Вы не выбрали файл<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>");
        }


    });
    $('#uploadFile2').click(function (event) {

// Создадим данные формы и добавим в них данные файлов из files
        if (files !== undefined) {

            var data = new FormData();
            $.each(files, function (key, value) {
                data.append(key, value);
            });

            var essence = $(this).attr('essence');
            //Добаваляем id сущности
            data.append('id_es', $('input[name=id]').val());
            // Отправляем запрос

            $.ajax({
                url: '/admin/' + essence + '/addFile',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Не обрабатываем файлы (Don't process the files)
                contentType: false, // Так jQuery скажет серверу что это строковой запрос
                success: function (respond) {
                    $('#modal_result_id_file').html("<div class='alert alert-warning fade in' id='result_div_id'>" + respond + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>");
                    $.get(
                        "/file/get",
                        {
                            essence: essence,
                            id_es: $('input[name=id]').val()
                        },
                        function (data) {
                            var html = '';
                            var x = data.length;
                            if (x) {

                                html += '<tr><td>Тип</td><td>Название</td></tr>';


                                while (x--) {
                                    html += '<tr id="fileBlock-' + data[x]['id'] + '"><td width="100"><img width="50" src="/public/images/' + data[x]['type'] + '.png"';
                                    html += 'title="' + data[x]['type'] + '" alt="' + data[x]['type'] + '" /></td>';
                                    html +=  '<td align="left" style="vertical-align: middle;"><a target="_blank" href="/public/files/' + data[x]['file'] + '">';
                                    html +=  '<strong>' + data[x]['file'] + '</strong></a></td>';
                                    html += '<td class="fileDel" url="/admin/' + essence + '/delFile" idfile="' + data[x]['id'] + '"><a>Удалить</a></td></tr>';

                                }

                                $('#fileContainer').html(html);
                                $('.fileDel').click(function () {
                                    delFile(this);
                                });
                            }
                        }, "json");
                    $('input[type=file]').val('');
                }
            });
        } else {

            $('#modal_result_id').html("<div class='alert alert-warning fade in' id='result_div_id'>Вы не выбрали файл<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>");
        }


    });
});

