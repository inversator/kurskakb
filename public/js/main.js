var $RoomWrap = jQuery.noConflict();
function AjaxFormRequest(result_id, form_id, url) {
    jQuery.ajax({
        url: url, //Адрес подгружаемой страницы
        type: "POST", //Тип запроса
        dataType: "html", //Тип данных
        data: jQuery("#" + form_id).serialize(),
        success: function (response) { //Если все нормально
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        },
        error: function (response) { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}

function AjaxResForm(result_id, form_id, url, resp) {

    var resp = resp || 'Сообщение отправлено';

    jQuery.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: jQuery("#" + form_id).serialize(),
        success: function (response) {

            if (response[0] !== 0) {
                document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + resp + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
                $RoomWrap('#' + form_id).remove();
            } else {
                var message = response[1];

                if (message !== undefined) {
                    var goodFields = arrDiff(message, jQuery("#" + form_id).serialize());

                    for (var field in goodFields) {
                        $RoomWrap('#' + form_id + ' div[own="' + field + '"]').attr('class', 'form-group');
                        $RoomWrap('#' + form_id + ' div[own="' + field + '"] error').html('');
                    }

                    for (var key in message) {
                        $RoomWrap('#' + form_id + ' div[own="' + key + '"]').attr('class', 'form-group has-error');
                        $RoomWrap('#' + form_id + ' div[own="' + key + '"] error').html(message[key]);
                    }
                }
            }
        },
        error: function () { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}

function AjaxRegForm(result_id, form_id, url) {
    jQuery.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: jQuery("#" + form_id).serialize(),
        success: function (response) {

            if (!response[0]) {
                document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response[1] + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
            } else {
                window.location.replace(response[1]);
            }
        },
        error: function () { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}

function addToCart(id, count) {
    $RoomWrap.post(
        '/cart/addToCart',
        {
            id: id,
            count: count
        },
        addCartSuccess
    );
}

function addCartSuccess(data) {
    updateCart();
}
function updateCart() {
    $RoomWrap.get("/cart/updateCart", function (html) {
            $RoomWrap('#cart').html(html['cart']);
            $RoomWrap('.fullCartBlock').html(html['block']);
        }, "json"
    );
}
function delFromCart(id) {
    $RoomWrap.ajax({
        url: "/cart/delFromCart",
        async: false,
        data: {id: id},
        success: function () {
            updateCart();
        }
    });
}

function changeCountCart(id, sign) {
    $RoomWrap.ajax({
        url: "/cart/changeCount",
        async: false,
        data: {id: id, sign: sign},
        success: function () {
            updateCart();
        }
    });
}

function makeOrder() {
    if ($RoomWrap('input[name="call"]').prop("checked"))
        call = 1;
    else
        call = 0;
    var data = {
        id: '',
        name: $RoomWrap('input[name="name"]').val(),
        phone: $RoomWrap('input[name="phone"]').val(),
        email: $RoomWrap('input[name="email"]').val(),
        index: $RoomWrap('input[name="index"]').val(),
        city: $RoomWrap('input[name="city"]').val(),
        address: $RoomWrap('textarea[name="address"]').val(),
        comment: $RoomWrap('textarea[name="comment"]').val(),
        call: call,
        delivery: $RoomWrap('input[name="delivery"]:checked').val()
    };

    $RoomWrap.ajax({
        url: "/order/make",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {
            if (message['result']) {
                var html = '';

                html += '<div class="alert alert-success">';
                html += '<strong>Заказ оформлен!</strong> Ждите сообщения или звонка от наших менеджеров.';
                html += '</div>';

                $RoomWrap('#accordion').html(html);
                $RoomWrap.get("/cart/clearCart", function () {
                    updateCart();
                });

            } else {
                if (message['error'] !== undefined) {
                    for (var key in message['error']) {
                        $RoomWrap('div[id="' + key + '"]').attr('class', 'form-group has-error');
                    }
                }
            }
        }
    });

}

function checkField(idForm) {

    var data = {
        name: $RoomWrap('#' + idForm + ' input[name="name"]').val(),
        phone: $RoomWrap('#' + idForm + ' input[name="phone"]').val(),
        email: $RoomWrap('#' + idForm + ' input[name="email"]').val(),
        comment: $RoomWrap('#' + idForm + ' textarea[name="comment"]').val()
    };

    $RoomWrap.ajax({
        url: "sender/check",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {
            if (message !== undefined && message !== 0) {
                var goodFields = arrDiff(message, data);

                for (var field in goodFields) {
                    $RoomWrap('#' + idForm + ' div[own="' + field + '"]').attr('class', 'form-group');
                    $RoomWrap('#' + idForm + ' div[own="' + field + '"] error').html('');
                }

                for (var key in message) {
                    $RoomWrap('#' + idForm + ' div[own="' + key + '"]').attr('class', 'form-group has-error');
                    $RoomWrap('#' + idForm + ' div[own="' + key + '"] error').html(message[key]);
                }
            } else {
                for (var key in data) {
                    $RoomWrap('#' + idForm + ' div[own="' + key + '"]').attr('class', 'form-group');
                    $RoomWrap('#' + idForm + ' div[own="' + key + '"] error').html('');
                }
            }
        }
    });
}


function arrDiff(arr1, arr2) {
    for (var key in arr1) {
        if (arr2[key] !== undefined) {
            delete arr2[key];
        }
    }
    return arr2;
}

function changeFilter() {

    var filters = {};

    $RoomWrap('.filter input[name="check[]"]:checkbox:checked').each(function () {
        if (filters[$RoomWrap(this).attr('id')] == undefined)
            filters[$RoomWrap(this).attr('id')] = {};
        filters[$RoomWrap(this).attr('id')][$RoomWrap(this).attr('num')] = $RoomWrap(this).val();
    });

    var page = $RoomWrap.getUrlVar('page') !== undefined ? $RoomWrap.getUrlVar('page') : 1;
    var sort = $RoomWrap.getUrlVar('order') !== undefined ? $RoomWrap.getUrlVar('order') : 'id';

    var data = {
        cat: $RoomWrap('.filter').attr('cat'),
        slug: $RoomWrap('.filter').attr('slug'),
        filters: filters,
        sort: sort,
        page: page
    }

    $RoomWrap.ajax({
        url: '/category/filters',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (view) {

            $RoomWrap('#unitsBlock').html(view['units']);
            $RoomWrap('.paginationBlock').html(view['pagination']);
        }
    });
}

$RoomWrap.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $RoomWrap.getUrlVars()[name];
    }
});

function bookIt() {
    var dateIn = $RoomWrap('#dateIn').val().replace(/[.]/g, '%2F');
    var dateOut = $RoomWrap('#dateOut').val().replace(/[.]/g, '%2F');

    window.open('http://hostel24-ryazan-book.otelms.com/bookit/step1/?lang=ru&datein=' + dateIn + '&dateout=' + dateOut + '#anhor1', '_blank');
}
function applySort(sort) {
    var page = $RoomWrap.getUrlVar('page') !== undefined ? $RoomWrap.getUrlVar('page') : 1;

    window.history.pushState('1', 'Title', '?' + sort + '&page=' + page);

    $RoomWrap('#sort button').attr('class', 'btn btn-default btn-xs');

    if (sort === 'order=price DESC') {
        $RoomWrap('#sort #price-desc').attr('class', 'btn btn-primary btn-xs');
    } else if (sort === 'order=price') {
        $RoomWrap('#sort #price').attr('class', 'btn btn-primary btn-xs');
    } else {
        $RoomWrap('#sort #auto').attr('class', 'btn btn-primary btn-xs');
    }

    var filters = {};

    $RoomWrap('.filter input[name="check[]"]:checkbox:checked').each(function () {
        if (filters[$RoomWrap(this).attr('id')] == undefined)
            filters[$RoomWrap(this).attr('id')] = {};
        filters[$RoomWrap(this).attr('id')][$RoomWrap(this).attr('num')] = $RoomWrap(this).val();
    });


    var sort = $RoomWrap.getUrlVar('order') !== undefined ? $RoomWrap.getUrlVar('order') : 'id';

    var data = {
        cat: $RoomWrap('.filter').attr('cat'),
        slug: $RoomWrap('.filter').attr('slug'),
        filters: filters,
        sort: sort,
        page: page
    };

    $RoomWrap.ajax({
        url: '/category/filters',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (view) {
            $RoomWrap('#unitsBlock').html(view['units']);
            $RoomWrap('.paginationBlock').html(view['pagination']);
        }
    });
}

function jumpPag(page) {
    var sort = $RoomWrap.getUrlVar('order') !== undefined ? $RoomWrap.getUrlVar('order') : 'id';

    window.history.pushState('1', 'Title', '?page=' + page + '&order=' + sort);

    var filters = {};

    $RoomWrap('.filter input[name="check[]"]:checkbox:checked').each(function () {
        if (filters[$RoomWrap(this).attr('id')] == undefined)
            filters[$RoomWrap(this).attr('id')] = {};
        filters[$RoomWrap(this).attr('id')][$RoomWrap(this).attr('num')] = $RoomWrap(this).val();
    });

    var page = $RoomWrap.getUrlVar('page') !== undefined ? $RoomWrap.getUrlVar('page') : 1;

    var data = {
        cat: $RoomWrap('.filter').attr('cat'),
        slug: $RoomWrap('.filter').attr('slug'),
        filters: filters,
        sort: sort,
        page: page
    };

    var type = $RoomWrap('.filter').attr('type') || 'category';

    $RoomWrap.ajax({
        url: '/' + type + '/filters',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (view) {

            $RoomWrap('#unitsBlock').html(view['units']);
            $RoomWrap('.paginationBlock').html(view['pagination']);
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' + error);
        }
    });

}
$RoomWrap(document).ready(function () {

// Запускаем пикер
    $RoomWrap.datetimepicker.setLocale('ru');
    $RoomWrap('#dateIn').datetimepicker({
        timepicker: false,
        minDate: '-1970/01/01',
        maxDate: '+1970/01/30',
        onChangeDateTime: function (dp, $input) {
            $RoomWrap('#dateOut').datetimepicker({minDate: $input.val()});
        },
        dayOfWeekStart: 1,
        format: 'd.m.Y'
    });
    $RoomWrap('#dateOut').datetimepicker({
        timepicker: false,
        minDate: '+1970/01/02',
        maxDate: '+1970/01/30',
        dayOfWeekStart: 1,
        format: 'd.m.Y'
    });

    $RoomWrap(document).ready(function () {

        var $menu = $RoomWrap("#scroll-menu");

        $RoomWrap(window).scroll(function () {
            if ($RoomWrap(this).scrollTop() > 90 && $menu.hasClass("navbar-static-top")) {
                $menu.removeClass("navbar-static-top").addClass("navbar-fixed-top");
            } else if ($RoomWrap(this).scrollTop() <= 90 && $menu.hasClass("navbar-fixed-top")) {
                $menu.removeClass("navbar-fixed-top").addClass("navbar-static-top");
            }
        });//scroll
    });

});
/* Функция превращения времени в TimeStamp 
 * в формате 'd.m.Y H:i'
 * */

function ToTomestamp(datetime) {
    if (datetime) {
        var dateString = datetime,
            dateParts = dateString.split(' '),
            timeParts = dateParts[1].split(':'),
            date;
        dateParts = dateParts[0].split('.');
        dateTimeBlock = new Date(dateParts[2], parseInt(dateParts[1], 10) - 1, dateParts[0], timeParts[0], timeParts[1]);
        date = dateTimeBlock.getTime();
        date = date.toString();
        date = date.substring(0, date.length - 3);
    } else {
        date = jQuery.now().substring(0, jQuery.now().length - 3);
    }

    return date;
}